import 'package:organikart_admin/presentation/auth/splash.dart';
import 'package:organikart_admin/presentation/auth/login.dart';
import 'package:organikart_admin/presentation/screens/landingPage.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

@MaterialAutoRouter(
    generateNavigationHelperExtension: true,
    preferRelativeImports: false,
    pathPrefix: "",
    routesClassName: 'Routes',
    routes: <AutoRoute>[
      MaterialRoute(page: SplashView, initial: true),
      MaterialRoute(page: Login),
      MaterialRoute(
        page: LandingPage,
      ),
    ])
class $AppRouter {}
