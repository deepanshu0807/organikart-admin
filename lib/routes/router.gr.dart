// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:organikart_admin/presentation/auth/login.dart';
import 'package:organikart_admin/presentation/auth/splash.dart';
import 'package:organikart_admin/presentation/screens/landingPage.dart';

class Routes {
  static const String splashView = '/';
  static const String login = 'Login';
  static const String landingPage = 'landing-page';
  static const all = <String>{
    splashView,
    login,
    landingPage,
  };
}

class AppRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.splashView, page: SplashView),
    RouteDef(Routes.login, page: Login),
    RouteDef(Routes.landingPage, page: LandingPage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    SplashView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SplashView(),
        settings: data,
      );
    },
    Login: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => Login(),
        settings: data,
      );
    },
    LandingPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => LandingPage(),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Navigation helper methods extension
/// *************************************************************************

extension AppRouterExtendedNavigatorStateX on ExtendedNavigatorState {
  Future<dynamic> pushSplashView() => push<dynamic>(Routes.splashView);

  Future<dynamic> pushLogin() => push<dynamic>(Routes.login);

  Future<dynamic> pushLandingPage() => push<dynamic>(Routes.landingPage);
}
