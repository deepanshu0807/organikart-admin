import 'package:flutter/material.dart';

class TranslateUpOnHover extends StatefulWidget {
  final Widget child;

  TranslateUpOnHover({Key key, this.child}) : super(key: key);

  @override
  _TranslateUpOnHoverState createState() => _TranslateUpOnHoverState();
}

class _TranslateUpOnHoverState extends State<TranslateUpOnHover> {
  final nonHoverTransform = Matrix4.identity()..translate(0, 0, 0);
  final hoverTransform = Matrix4.identity()..translate(0, -10, 0);

  bool _hovering = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (e) => _mouseEnter(true),
      onExit: (e) => _mouseEnter(false),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 200),
        child: widget.child,
        transform: _hovering ? hoverTransform : nonHoverTransform,
      ),
    );
  }

  void _mouseEnter(bool hover) {
    setState(() {
      _hovering = hover;
    });
  }
}

class TranslateRightOnHover extends StatefulWidget {
  final Widget child;

  TranslateRightOnHover({Key key, this.child}) : super(key: key);

  @override
  _TranslateRightOnHoverState createState() => _TranslateRightOnHoverState();
}

class _TranslateRightOnHoverState extends State<TranslateRightOnHover> {
  final nonHoverTransform = Matrix4.identity()..translate(0, 0, 0);
  final hoverTransform = Matrix4.identity()
    ..translate(10, 0, 0)
    ..scale(1.2, 1);

  bool _hovering = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (e) => _mouseEnter(true),
      onExit: (e) => _mouseEnter(false),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 200),
        child: widget.child,
        transform: _hovering ? hoverTransform : nonHoverTransform,
      ),
    );
  }

  void _mouseEnter(bool hover) {
    setState(() {
      _hovering = hover;
    });
  }
}

class TranslateLeftOnHover extends StatefulWidget {
  final Widget child;

  TranslateLeftOnHover({Key key, this.child}) : super(key: key);

  @override
  _TranslateLeftOnHoverState createState() => _TranslateLeftOnHoverState();
}

class _TranslateLeftOnHoverState extends State<TranslateLeftOnHover> {
  final nonHoverTransform = Matrix4.identity()..translate(0, 0, 0);
  final hoverTransform = Matrix4.identity()..translate(-10, 0, 0);

  bool _hovering = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (e) => _mouseEnter(true),
      onExit: (e) => _mouseEnter(false),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 200),
        child: widget.child,
        transform: _hovering ? hoverTransform : nonHoverTransform,
      ),
    );
  }

  void _mouseEnter(bool hover) {
    setState(() {
      _hovering = hover;
    });
  }
}

class TranslateDownOnHover extends StatefulWidget {
  final Widget child;

  TranslateDownOnHover({Key key, this.child}) : super(key: key);

  @override
  _TranslateDownOnHoverState createState() => _TranslateDownOnHoverState();
}

class _TranslateDownOnHoverState extends State<TranslateDownOnHover> {
  final nonHoverTransform = Matrix4.identity()..translate(0, 0, 0);
  final hoverTransform = Matrix4.identity()..translate(0, 10, 0);

  bool _hovering = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (e) => _mouseEnter(true),
      onExit: (e) => _mouseEnter(false),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 200),
        child: widget.child,
        transform: _hovering ? hoverTransform : nonHoverTransform,
      ),
    );
  }

  void _mouseEnter(bool hover) {
    setState(() {
      _hovering = hover;
    });
  }
}

class TranslateColorOnHover extends StatefulWidget {
  final Widget child;

  TranslateColorOnHover({Key key, this.child}) : super(key: key);

  @override
  _TranslateColorOnHoverState createState() => _TranslateColorOnHoverState();
}

class _TranslateColorOnHoverState extends State<TranslateColorOnHover> {
  final nonHoverColor = Colors.transparent;
  final hoverColor = Colors.black;

  bool _hovering = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (e) => _mouseEnter(true),
      onExit: (e) => _mouseEnter(false),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 200),
        child: widget.child,
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: _hovering ? hoverColor : nonHoverColor,
            blurRadius: 4,
            spreadRadius: 4,
          )
        ]),
      ),
    );
  }

  void _mouseEnter(bool hover) {
    setState(() {
      _hovering = hover;
    });
  }
}
