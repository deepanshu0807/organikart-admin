import 'package:flutter/material.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 80,
        width: 80,
        child: CircularProgressIndicator(
          backgroundColor: AppColors.getPrimaryColor().withOpacity(0.3),
          valueColor:
              AlwaysStoppedAnimation<Color>(AppColors.getPrimaryColor()),
          strokeWidth: 3,
        ),
      ),
    );
  }
}
