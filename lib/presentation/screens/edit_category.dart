import 'package:cached_network_image/cached_network_image.dart';

import 'package:flutter/material.dart';
import 'package:organikart_admin/application/category_form_bloc/category_form_bloc.dart';
import 'package:organikart_admin/presentation/widgets/loading.dart';
import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:organikart_admin/presentation/widgets/hover_extension.dart';

ScrollController scrollController = ScrollController();

class EditCategoryView extends StatefulWidget {
  final Category category;

  const EditCategoryView({Key key, this.category}) : super(key: key);
  @override
  _EditCategoryViewState createState() => _EditCategoryViewState();
}

class _EditCategoryViewState extends State<EditCategoryView> {
  TextEditingController _title = TextEditingController();
  TextEditingController _shortDescription = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final category = widget.category;

    context
        .read<CategoryFormBloc>()
        .add(CategoryFormEvent.selectedForEditing(category));

    if (category?.title?.isNotEmpty ?? false) {
      _title = TextEditingController(text: category.title);
    }

    if (category?.shortDescription?.isNotEmpty ?? false) {
      _shortDescription =
          TextEditingController(text: category.shortDescription);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.getBlack(),
      body: BlocConsumer<CategoryFormBloc, CategoryFormState>(
        listener: (context, state) {
          state.titleImageUploadFailureOrSuccessOption.fold(
            () {},
            (i) => i.fold(
              (l) => l.maybeMap(
                unexpected: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                insufficientPermissions: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                serverError: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                invalidData: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.invalidData),
                orElse: () {},
              ),
              (r) {
                // debugPrint("data is saved");
                DisplayMessage.showSuccessMessage(context, "Image is uploaded");
                context
                    .read<CategoryFormBloc>()
                    .add(CategoryFormEvent.titlePicUrlChanged(r.picUrl));
              },
            ),
          );

          state.saveFailureOrSuccessOption.fold(
            () {},
            (i) => i.fold(
              (l) => l.maybeMap(
                  unexpected: (_) => DisplayMessage.showErrorMessage(
                      context, DisplayMessage.serverError),
                  insufficientPermissions: (_) =>
                      DisplayMessage.showErrorMessage(
                          context, DisplayMessage.serverError),
                  serverError: (_) => DisplayMessage.showErrorMessage(
                      context, DisplayMessage.serverError),
                  invalidData: (_) => DisplayMessage.showErrorMessage(
                      context, DisplayMessage.invalidData),
                  orElse: () {}),
              (r) {
                DisplayMessage.showSuccessMessage(
                    context, "Category is updated");
                Future.delayed(Duration(seconds: 2),
                    () => ExtendedNavigator.of(context).pop());
              },
            ),
          );

          state.deleteFailureOrSuccessOption.fold(
            () {},
            (i) => i.fold(
              (l) => l.maybeMap(
                unexpected: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                insufficientPermissions: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                serverError: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                invalidData: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.invalidData),
                orElse: () {},
              ),
              (r) {
                DisplayMessage.showSuccessMessage(
                    context, "Deleted successfully");

                Future.delayed(Duration(seconds: 2),
                    () => ExtendedNavigator.of(context).pop());
              },
            ),
          );
        },
        builder: (context, state) {
          Category category = state.category;
          return Stack(
            children: [
              Positioned(
                  top: 15.h,
                  left: 40.w,
                  child: Text(
                    "Organikart",
                    style: text80.copyWith(
                        fontSize: 50.sp, fontWeight: FontWeight.bold),
                  )),
              Positioned(
                top: 15.h,
                right: 40.w,
                child: Text(
                  "Edit Category",
                  style: text30.copyWith(fontSize: 40.sp),
                ).showCursorOnHover.changeShadowColorOnHover,
              ),
              Positioned(
                top: 90.h,
                left: 30.w,
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios_sharp,
                    color: AppColors.getPrimaryColor(),
                    size: 50,
                  ).showCursorOnHover.moveLeftOnHover,
                ),
              ),
              Positioned(
                  left: 100.w,
                  right: 100.w,
                  top: 0,
                  bottom: 0,
                  child: ListView(
                    controller: scrollController,
                    children: [
                      SizedBox(
                        height: 200.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 600.w,
                                child: TextFormField(
                                  controller: _title,
                                  cursorColor: AppColors.getPrimaryColor(),
                                  textAlign: TextAlign.center,
                                  style: text80,
                                  decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColors.getPrimaryColor()),
                                    ),
                                    hintText: 'Name',
                                    hintStyle: text80.copyWith(
                                        color: AppColors.getPrimaryColor()
                                            .withOpacity(0.5)),
                                  ),
                                  onChanged: (value) {
                                    context.read<CategoryFormBloc>().add(
                                        CategoryFormEvent.titleChanged(value));
                                  },
                                ),
                              ),
                              verticalSpaceMedium30,
                              Container(
                                height: 300.h,
                                width: 600.w,
                                padding: EdgeInsets.all(8.h),
                                alignment: Alignment.topCenter,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: AppColors.getPrimaryColor(),
                                        width: 2)),
                                child: TextFormField(
                                  controller: _shortDescription,
                                  cursorColor: AppColors.getPrimaryColor(),
                                  textAlign: TextAlign.center,
                                  style: text30,
                                  maxLines: 7,
                                  decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColors.getPrimaryColor()),
                                    ),
                                    hintText: 'Description / About',
                                    hintStyle: text30.copyWith(
                                        color: AppColors.getPrimaryColor()
                                            .withOpacity(0.5)),
                                  ),
                                  onChanged: (value) {
                                    context.read<CategoryFormBloc>().add(
                                        CategoryFormEvent
                                            .shortDescriptionChanged(value));
                                  },
                                ),
                              )
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Stack(
                                children: [
                                  Container(
                                    height: 600.h,
                                    width: 500.w,
                                    decoration: BoxDecoration(
                                      color: AppColors.getSecondaryColor(),
                                    ),
                                    child: state.isLoadingTitleImage
                                        ? Loading()
                                        : CachedNetworkImage(
                                            imageUrl: category.titlePicUrl,
                                            fit: BoxFit.cover,
                                            placeholder: (context, url) =>
                                                Container(
                                                    height: 730.h,
                                                    width: 500.w,
                                                    color: AppColors
                                                        .getSecondaryColor()),
                                            errorWidget: (context, url,
                                                    error) =>
                                                Container(
                                                    height: 730.h,
                                                    width: 500.w,
                                                    color: AppColors
                                                        .getSecondaryColor()),
                                          ),
                                  ),
                                  Positioned(
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                      child: FlatButton(
                                        height: 50,
                                        onPressed: () {
                                          context.read<CategoryFormBloc>().add(
                                              CategoryFormEvent
                                                  .uploadTitleImageClicked());
                                        },
                                        color: AppColors.getPrimaryColor(),
                                        child: Text(
                                          "Upload Image",
                                          style: text30.copyWith(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ))
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                      verticalSpaceLarge,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FlatButton(
                            height: 55,
                            minWidth: 120,
                            padding: EdgeInsets.symmetric(horizontal: 30),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            onPressed: state.isLoadingTitleImage
                                ? null
                                : () {
                                    context
                                        .read<CategoryFormBloc>()
                                        .add(CategoryFormEvent.saveIsClicked());
                                  },
                            color: AppColors.getPrimaryColor(),
                            child: Text(
                              "Save Category",
                              style: text35.copyWith(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          horizontalSpaceMedium40,
                          FlatButton(
                            height: 55,
                            minWidth: 120,
                            padding: EdgeInsets.symmetric(horizontal: 30),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            onPressed: state.isLoadingTitleImage
                                ? null
                                : () {
                                    context.read<CategoryFormBloc>().add(
                                        CategoryFormEvent.deleteIsClicked(
                                            category));
                                  },
                            color: AppColors.getPrimaryColor(),
                            child: Text(
                              "Delete Category",
                              style: text35.copyWith(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      )
                    ],
                  ))
            ],
          );
        },
      ),
    );
  }
}
