import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:organikart_admin/application/auth/auth_watcher_bloc/auth_watcher_bloc.dart';
import 'package:organikart_admin/application/category_form_bloc/category_form_bloc.dart';
import 'package:organikart_admin/application/content_form_bloc/content_form_bloc.dart';
import 'package:organikart_admin/presentation/widgets/loading.dart';
import 'package:organikart_admin/routes/router.gr.dart';
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:organikart_admin/presentation/widgets/hover_extension.dart';

import 'add_category.dart';
import 'add_product.dart';
import 'edit_category.dart';
import 'edit_product.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.getBlack(),
      body: Stack(
        children: [
          Positioned(
              top: 15.h,
              left: 40.w,
              child: Text(
                "Organikart",
                style: text80.copyWith(
                    fontSize: 50.sp, fontWeight: FontWeight.bold),
              )),
          Positioned(
              bottom: 20.h,
              left: 40.w,
              child: BlocBuilder<AuthWatcherBloc, AuthWatcherState>(
                  builder: (context, state) {
                return InkWell(
                  onTap: state.map(
                    initial: (_) => null,
                    authenticated: (user) => () {
                      context
                          .read<AuthWatcherBloc>()
                          .add(AuthWatcherEvent.signedOut(user.user));
                      ExtendedNavigator.of(context)
                          .pushAndRemoveUntil(Routes.login, (route) => false);
                    },
                    unauthenticated: (_) => null,
                  ),
                  child: Text(
                    "Logout ",
                    style: text30.copyWith(fontWeight: FontWeight.bold),
                  ).showCursorOnHover.moveUpOnHover,
                );
              })),
          Positioned(
            top: 90.h,
            bottom: 90.h,
            left: 150.w,
            right: 150.w,
            child: Center(
              child: Container(
                  alignment: Alignment.topCenter,
                  height: 840.h,
                  width: 1460.w,
                  color: Colors.white10,
                  padding: EdgeInsets.fromLTRB(40.w, 40.h, 40.w, 40.w),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Wrap(
                        spacing: 50.w,
                        runSpacing: 80.h,
                        children: [
                          CardsWidget(
                            title: "Add Category",
                            subTitle: "Add new category to the collection",
                            onTap: () {
                              Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                      builder: (context) => AddCategoryView()));
                            },
                          ),
                          CardsWidget(
                            title: "Edit Category",
                            subTitle:
                                "Edit details of Category present in database",
                            onTap: () {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return ShowCategoryListDialog();
                                },
                              );
                            },
                          ),
                          CardsWidget(
                            title: "Add Product",
                            subTitle: "Add product in a Category",
                            onTap: () {
                              Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                      builder: (context) => AddProductView()));
                            },
                          ),
                          CardsWidget(
                            title: "Edit Product",
                            subTitle: "Edit product from a category",
                            onTap: () {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return ShowProductsListDialog();
                                },
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}

class CardsWidget extends StatelessWidget {
  final String title;
  final String subTitle;
  final Function onTap;
  const CardsWidget({
    Key key,
    this.title,
    this.subTitle,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      highlightColor: Colors.transparent,
      child: Container(
        height: 200,
        width: 320,
        padding: EdgeInsets.symmetric(vertical: 30.h, horizontal: 40.w),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: AppColors.getPrimaryColor().withOpacity(0.8)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: text60.copyWith(color: Colors.black),
            ),
            Text(
              subTitle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: text35.copyWith(color: AppColors.getSecondaryColor()),
            )
          ],
        ),
      ).showCursorOnHover.moveUpOnHover,
    );
  }
}

class ShowCategoryListDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: AppColors.getPrimaryColor().withOpacity(0.4),
      child: Container(
        margin: EdgeInsets.all(30.w),
        padding: EdgeInsets.all(50.h),
        width: screenWidth(context) / 2.5,
        color: AppColors.getPrimaryColor(),
        child: SingleChildScrollView(
          primary: true,
          child: Column(
            children: [
              Text(
                "Edit Category",
                style: text60.copyWith(color: Colors.black),
              ),
              verticalSpaceMedium30,
              SizedBox(
                width: double.infinity,
                height: screenHeight(context) / 1.5,
                child: BlocBuilder<CategoryWatcherBloc, CategoryWatcherState>(
                  builder: (context, state) {
                    return state.map(
                      initial: (_) => Loading(),
                      loadInProgress: (_) => Loading(),
                      loadFailure: (_) => Text("Err"),
                      loadSuccess: (c) {
                        if (c.categories.isEmpty) {
                          return Center(
                            child: Text(
                              "No category found. Add a category first!",
                              style: text30.copyWith(color: Colors.black),
                            ),
                          );
                        } else {
                          return ListView.builder(
                            itemCount: c.categories.length,
                            itemBuilder: (context, index) {
                              final thisCategory = c.categories[index];
                              return InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (context) =>
                                              EditCategoryView(
                                                category: thisCategory,
                                              )));
                                },
                                highlightColor: Colors.transparent,
                                child: Container(
                                  height: 50,
                                  width: double.infinity,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 20.h, horizontal: 80.w),
                                  padding: EdgeInsets.symmetric(
                                      vertical: 8.h, horizontal: 20.w),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: AppColors.getSecondaryColor()),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(thisCategory.title, style: text30),
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.edit,
                                            color: AppColors.getPrimaryColor(),
                                          ),
                                          horizontalSpaceMedium20,
                                          InkWell(
                                            onTap: () {
                                              context
                                                  .read<CategoryFormBloc>()
                                                  .add(CategoryFormEvent
                                                      .deleteIsClicked(
                                                          thisCategory));
                                            },
                                            child: Icon(
                                              Icons.delete,
                                              color:
                                                  AppColors.getPrimaryColor(),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        }
                      },
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ShowProductsListDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: AppColors.getPrimaryColor().withOpacity(0.4),
      child: Container(
        margin: EdgeInsets.all(30.w),
        padding: EdgeInsets.all(50.h),
        width: screenWidth(context) / 2.5,
        color: AppColors.getPrimaryColor(),
        child: SingleChildScrollView(
          primary: true,
          child: Column(
            children: [
              Text(
                "Edit Product",
                style: text60.copyWith(color: Colors.black),
              ),
              verticalSpaceMedium30,
              SizedBox(
                width: double.infinity,
                height: screenHeight(context) / 1.5,
                child: BlocBuilder<ContentWatcherBloc, ContentWatcherState>(
                  builder: (context, state) {
                    return state.map(
                      initial: (_) => Loading(),
                      loadInProgress: (_) => Loading(),
                      loadFailure: (_) => Text("Err"),
                      loadSuccess: (c) {
                        if (c.content.isEmpty) {
                          return Center(
                            child: Text(
                              "No product found. Add some products first!",
                              style: text30.copyWith(color: Colors.black),
                            ),
                          );
                        } else {
                          return ListView.builder(
                            itemCount: c.content.length,
                            itemBuilder: (context, index) {
                              final thisContent = c.content[index];
                              return InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (context) => EditProductView(
                                                content: thisContent,
                                              )));
                                },
                                highlightColor: Colors.transparent,
                                child: Container(
                                  height: 80,
                                  width: double.infinity,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 20.h, horizontal: 80.w),
                                  padding: EdgeInsets.symmetric(
                                      vertical: 8.h, horizontal: 20.w),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: AppColors.getSecondaryColor()),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(thisContent.name, style: text35),
                                          Row(
                                            children: [
                                              Icon(
                                                Icons.edit,
                                                color:
                                                    AppColors.getPrimaryColor(),
                                              ),
                                              horizontalSpaceMedium20,
                                              InkWell(
                                                onTap: () {
                                                  context
                                                      .read<ContentFormBloc>()
                                                      .add(ContentFormEvent
                                                          .deleteIsClicked(
                                                              thisContent));
                                                },
                                                child: Icon(
                                                  Icons.delete,
                                                  color: AppColors
                                                      .getPrimaryColor(),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                      Text(
                                        "under ${thisContent.category.title}",
                                        style: text25,
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        }
                      },
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
