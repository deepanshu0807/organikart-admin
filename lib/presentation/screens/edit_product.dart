import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:organikart_admin/application/content_form_bloc/content_form_bloc.dart';
import 'package:organikart_admin/presentation/widgets/loading.dart';
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart';
import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:organikart_admin/presentation/widgets/hover_extension.dart';

ScrollController scrollController = ScrollController();

class EditProductView extends StatefulWidget {
  final Content content;

  const EditProductView({Key key, this.content}) : super(key: key);
  @override
  _EditProductViewState createState() => _EditProductViewState();
}

class _EditProductViewState extends State<EditProductView> {
  String selectedSection = "Select";
  TextEditingController _name = TextEditingController();
  TextEditingController _description = TextEditingController();
  TextEditingController _mrp = TextEditingController();
  TextEditingController _discountMrp = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final content = widget.content;

    context
        .read<ContentFormBloc>()
        .add(ContentFormEvent.selectedForEditing(content));

    if (content?.name?.isNotEmpty ?? false) {
      _name = TextEditingController(text: content.name);
    }

    if (content?.description?.isNotEmpty ?? false) {
      _description = TextEditingController(text: content.description);
    }

    _mrp.text = content.mrp.getOrElse(0).toString();
    _discountMrp.text = content.discountedMrp.getOrElse(0).toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.getBlack(),
      body: BlocConsumer<ContentFormBloc, ContentFormState>(
        listener: (context, state) {
          state.mainContentImageUploadFailureOrSuccessOption.fold(
            () {},
            (i) => i.fold(
              (l) => l.maybeMap(
                unexpected: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                insufficientPermissions: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                serverError: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                invalidData: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.invalidData),
                orElse: () {},
              ),
              (r) {
                // debugPrint("data is saved");
                DisplayMessage.showSuccessMessage(context, "Image is uploaded");
                context
                    .read<ContentFormBloc>()
                    .add(ContentFormEvent.picUrlChanged(r.picUrl));
              },
            ),
          );

          state.saveFailureOrSuccessOption.fold(
            () {},
            (i) => i.fold(
              (l) => l.maybeMap(
                  unexpected: (_) => DisplayMessage.showErrorMessage(
                      context, DisplayMessage.serverError),
                  insufficientPermissions: (_) =>
                      DisplayMessage.showErrorMessage(
                          context, DisplayMessage.serverError),
                  serverError: (_) => DisplayMessage.showErrorMessage(
                      context, DisplayMessage.serverError),
                  invalidData: (_) => DisplayMessage.showErrorMessage(
                      context, DisplayMessage.invalidData),
                  orElse: () {}),
              (r) {
                DisplayMessage.showSuccessMessage(
                    context, "Product is updated");
                Future.delayed(Duration(seconds: 2),
                    () => ExtendedNavigator.of(context).pop());
              },
            ),
          );

          state.deleteFailureOrSuccessOption.fold(
            () {},
            (i) => i.fold(
              (l) => l.maybeMap(
                unexpected: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                insufficientPermissions: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                serverError: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.serverError),
                invalidData: (_) => DisplayMessage.showErrorMessage(
                    context, DisplayMessage.invalidData),
                orElse: () {},
              ),
              (r) {
                DisplayMessage.showSuccessMessage(
                    context, "Deleted successfully");

                Future.delayed(Duration(seconds: 2),
                    () => ExtendedNavigator.of(context).pop());
              },
            ),
          );
        },
        builder: (context, state) {
          Content content = state.content;
          return Stack(
            children: [
              Positioned(
                  top: 15.h,
                  left: 40.w,
                  child: Text(
                    "Organikart",
                    style: text80.copyWith(
                        fontSize: 50.sp, fontWeight: FontWeight.bold),
                  )),
              Positioned(
                top: 15.h,
                right: 40.w,
                child: Text(
                  "Edit product",
                  style: text30.copyWith(fontSize: 40.sp),
                ).showCursorOnHover.changeShadowColorOnHover,
              ),
              Positioned(
                top: 90.h,
                left: 30.w,
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios_sharp,
                    color: AppColors.getPrimaryColor(),
                    size: 50,
                  ).showCursorOnHover.moveLeftOnHover,
                ),
              ),
              Positioned(
                  left: 100.w,
                  right: 100.w,
                  top: 0,
                  bottom: 0,
                  child: ListView(
                    controller: scrollController,
                    children: [
                      SizedBox(
                        height: 200.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 600.w,
                                child: TextFormField(
                                  controller: _name,
                                  cursorColor: AppColors.getPrimaryColor(),
                                  textAlign: TextAlign.center,
                                  style: text60,
                                  decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColors.getPrimaryColor()),
                                    ),
                                    hintText: 'Name',
                                    hintStyle: text60.copyWith(
                                        color: AppColors.getPrimaryColor()
                                            .withOpacity(0.5)),
                                  ),
                                  onChanged: (value) {
                                    context.read<ContentFormBloc>().add(
                                        ContentFormEvent.nameChanged(value));
                                  },
                                ),
                              ),
                              verticalSpaceLarge,
                              Container(
                                height: 90.h,
                                width: 600.w,
                                padding: EdgeInsets.all(8.h),
                                alignment: Alignment.topCenter,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: AppColors.getPrimaryColor(),
                                        width: 2)),
                                child: TextFormField(
                                  controller: _description,
                                  cursorColor: AppColors.getPrimaryColor(),
                                  textAlign: TextAlign.center,
                                  style: text35,
                                  maxLines: 1,
                                  decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColors.getPrimaryColor()),
                                    ),
                                    hintText: 'Description',
                                    hintStyle: text35.copyWith(
                                        color: AppColors.getPrimaryColor()
                                            .withOpacity(0.5)),
                                  ),
                                  onChanged: (value) {
                                    context.read<ContentFormBloc>().add(
                                        ContentFormEvent.descriptionChanged(
                                            value));
                                  },
                                ),
                              ),
                              verticalSpaceLarge,
                              Row(
                                children: [
                                  Text('₹ ', style: text35),
                                  SizedBox(
                                    width: 100.w,
                                    child: TextFormField(
                                      controller: _mrp,
                                      cursorColor: AppColors.getPrimaryColor(),
                                      textAlign: TextAlign.center,
                                      style: text35,
                                      decoration: InputDecoration(
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color:
                                                  AppColors.getPrimaryColor()),
                                        ),
                                        hintText: 'MRP',
                                        hintStyle: text30.copyWith(
                                            color: AppColors.getPrimaryColor()
                                                .withOpacity(0.5)),
                                      ),
                                      onChanged: (value) {
                                        context.read<ContentFormBloc>().add(
                                            ContentFormEvent.mrpChanged(value));
                                      },
                                      validator: (_) => context
                                          .read<ContentFormBloc>()
                                          .state
                                          .content
                                          .mrp
                                          .value
                                          .fold(
                                            (f) => f.maybeMap(
                                                inValidPrice: (_) =>
                                                    "Invalid Price",
                                                orElse: () => null),
                                            (_) => null,
                                          ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 60.w,
                                  ),
                                  Text('₹ ', style: text35),
                                  SizedBox(
                                    width: 200.w,
                                    child: TextFormField(
                                      controller: _discountMrp,
                                      cursorColor: AppColors.getPrimaryColor(),
                                      textAlign: TextAlign.center,
                                      style: text35,
                                      decoration: InputDecoration(
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color:
                                                  AppColors.getPrimaryColor()),
                                        ),
                                        hintText: 'Discount Price',
                                        hintStyle: text25.copyWith(
                                            color: AppColors.getPrimaryColor()
                                                .withOpacity(0.5)),
                                      ),
                                      onChanged: (value) {
                                        context.read<ContentFormBloc>().add(
                                            ContentFormEvent.discountMrpChanged(
                                                value));
                                      },
                                      validator: (_) => context
                                          .read<ContentFormBloc>()
                                          .state
                                          .content
                                          .mrp
                                          .value
                                          .fold(
                                            (f) => f.maybeMap(
                                                inValidPrice: (_) =>
                                                    "Invalid Price",
                                                orElse: () => null),
                                            (_) => null,
                                          ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 60.w,
                                  ),
                                  Text(
                                      '% ${state.content.discountPercentage.getOrElse(0)}',
                                      style: text35),
                                ],
                              ),
                              verticalSpaceLarge,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Category : ",
                                    style: text35,
                                  ),
                                  BlocBuilder<CategoryWatcherBloc,
                                      CategoryWatcherState>(
                                    builder: (context, state) {
                                      return state.map(
                                        initial: (_) => Loading(),
                                        loadInProgress: (_) => Loading(),
                                        loadFailure: (_) => Text("Err"),
                                        loadSuccess: (c) {
                                          if (c.categories.isEmpty) {
                                            return Center(
                                              child: Text(
                                                "No category saved",
                                                style: text30,
                                              ),
                                            );
                                          } else {
                                            List<Category> categories = [];

                                            categories = state.map(
                                              initial: (_) => [],
                                              loadInProgress: (_) => [],
                                              loadSuccess: (a) => a.categories,
                                              loadFailure: (_) => [],
                                            );

                                            return DropdownButtonHideUnderline(
                                              // ignore: missing_required_param
                                              child: Container(
                                                margin: EdgeInsets.symmetric(
                                                    horizontal: 20.w),
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 20),
                                                height: 45,
                                                // width: 80.w as double,
                                                decoration: BoxDecoration(
                                                    color: AppColors
                                                        .getPrimaryColor(),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                child:
                                                    DropdownButtonHideUnderline(
                                                  // ignore: missing_required_param
                                                  child:
                                                      DropdownButton<Category>(
                                                    hint: Text(
                                                      content?.category
                                                              ?.title ??
                                                          selectedSection,
                                                      style: text30.copyWith(
                                                          color: AppColors
                                                              .getSecondaryColor()),
                                                    ),
                                                    iconSize: 30,
                                                    icon: Icon(
                                                      Icons.keyboard_arrow_down,
                                                      color: AppColors
                                                          .getSecondaryColor(),
                                                    ),
                                                    items: [...categories].map(
                                                      (val) {
                                                        return DropdownMenuItem<
                                                            Category>(
                                                          value: val,
                                                          child: Text(
                                                            val.title,
                                                            style: text25.copyWith(
                                                                color: AppColors
                                                                    .getSecondaryColor()),
                                                          ),
                                                        );
                                                      },
                                                    ).toList(),
                                                    onChanged: (Category val) {
                                                      setState(() {
                                                        selectedSection =
                                                            val.title;
                                                      });
                                                      context
                                                          .read<
                                                              ContentFormBloc>()
                                                          .add(ContentFormEvent
                                                              .selectedCategory(
                                                                  val));
                                                    },
                                                  ),
                                                ),
                                              ),
                                            );
                                          }
                                        },
                                      );
                                    },
                                  ),
                                  SizedBox(
                                    width: 60.w,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Qty Unit : ",
                                        style: text35,
                                      ),
                                      SizedBox(
                                        width: 120.w,
                                        child: DropdownButtonHideUnderline(
                                          // ignore: missing_required_param
                                          child: Container(
                                            padding:
                                                const EdgeInsets.only(left: 2),
                                            height: 45,
                                            width: 120.w,
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.grey),
                                                color:
                                                    AppColors.getPrimaryColor(),
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: DropdownButtonHideUnderline(
                                              // ignore: missing_required_param
                                              child: DropdownButton<QtyUnit>(
                                                hint: Text(
                                                  state.content?.qtyUnit
                                                          ?.toValueString() ??
                                                      "Kg",
                                                  style: text30.copyWith(
                                                      color: AppColors
                                                          .getSecondaryColor()),
                                                ),
                                                icon: const Icon(
                                                  Icons.keyboard_arrow_down,
                                                  color: Colors.grey,
                                                  size: 30,
                                                ),
                                                items: [
                                                  const QtyUnit.kg(),
                                                  const QtyUnit.gm(),
                                                  const QtyUnit.ltr(),
                                                  const QtyUnit.ml(),
                                                  const QtyUnit.dozen(),
                                                  const QtyUnit.piece()
                                                ].map(
                                                  (val) {
                                                    return DropdownMenuItem<
                                                        QtyUnit>(
                                                      value: val,
                                                      child: Text(
                                                          val.toValueString(),
                                                          style: text20.copyWith(
                                                              color: AppColors
                                                                  .getSecondaryColor())),
                                                    );
                                                  },
                                                ).toList(),
                                                onChanged: (QtyUnit val) {
                                                  context
                                                      .read<ContentFormBloc>()
                                                      .add(ContentFormEvent
                                                          .qtyUnitChanged(val));
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Stack(
                                children: [
                                  Container(
                                    height: 600.h,
                                    width: 500.w,
                                    decoration: BoxDecoration(
                                      color: AppColors.getSecondaryColor(),
                                    ),
                                    child: state.isLoadingMainContentImage
                                        ? Loading()
                                        : CachedNetworkImage(
                                            imageUrl: content.picUrl,
                                            fit: BoxFit.cover,
                                            placeholder: (context, url) =>
                                                Container(
                                                    height: 730.h,
                                                    width: 500.w,
                                                    color: AppColors
                                                        .getSecondaryColor()),
                                            errorWidget: (context, url,
                                                    error) =>
                                                Container(
                                                    height: 730.h,
                                                    width: 500.w,
                                                    color: AppColors
                                                        .getSecondaryColor()),
                                          ),
                                  ),
                                  Positioned(
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                      child: FlatButton(
                                        height: 50,
                                        onPressed: () {
                                          context.read<ContentFormBloc>().add(
                                              ContentFormEvent
                                                  .uploadImageClicked());
                                        },
                                        color: AppColors.getPrimaryColor(),
                                        child: Text(
                                          "Upload Image",
                                          style: text30.copyWith(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ))
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                      verticalSpaceLarge,
                      Center(
                        child: FlatButton(
                          height: 55,
                          minWidth: 120,
                          padding: EdgeInsets.symmetric(horizontal: 30),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: state.isLoadingMainContentImage
                              ? null
                              : () {
                                  context
                                      .read<ContentFormBloc>()
                                      .add(ContentFormEvent.saveIsClicked());
                                },
                          color: AppColors.getPrimaryColor(),
                          child: Text(
                            "Save Product",
                            style: text35.copyWith(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    ],
                  ))
            ],
          );
        },
      ),
    );
  }
}
