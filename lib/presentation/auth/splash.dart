import 'package:organikart_admin/application/auth/auth_watcher_bloc/auth_watcher_bloc.dart';
import 'package:organikart_admin/routes/router.gr.dart';
// import 'package:organikart_admin/routes/router.gr.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

import 'package:flutter/material.dart';

class SplashView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthWatcherBloc, AuthWatcherState>(
      listener: (context, state) {
        state.map(
          initial: (_) {
            debugPrint("state is initial");
          },
          authenticated: (_) {
            // debugPrint("state is authenticated");
            context
                .read<ContentWatcherBloc>()
                .add(ContentWatcherEvent.getContent());

            Future.delayed(const Duration(milliseconds: 1050), () {
              ExtendedNavigator.of(context)
                  .pushAndRemoveUntil(Routes.landingPage, (route) => false);
            });
          },
          unauthenticated: (_) {
            // debugPrint("state is unauthenticated");
            Future.delayed(
              const Duration(milliseconds: 1000),
              () {
                ExtendedNavigator.of(context).pushAndRemoveUntil(
                  Routes.login,
                  (route) => false,
                );
              },
            );
          },
        );
      },
      child: Scaffold(
        backgroundColor: AppColors.getBlack(),
        body: Container(
            height: screenHeight(context),
            width: screenWidth(context),
            alignment: Alignment.center,
            child: Text(
              "hello",
              style: text80.copyWith(fontWeight: FontWeight.bold),
            )),
      ),
    );
  }
}
