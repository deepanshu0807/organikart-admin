import 'package:organikart_admin/routes/router.gr.dart';
import 'package:organikart_admin/application/auth/signinform/signinform_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:organikart_admin/presentation/widgets/hover_extension.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.getBlack(),
      body: Stack(
        children: [
          Positioned(
            top: 90.h,
            bottom: 90.h,
            left: 150.w,
            right: 150.w,
            child: Center(
              child: Container(
                height: 840.h,
                width: 1460.w,
                color: Colors.white10,
                padding: EdgeInsets.all(35.w),
                child: LoginFormWidget(),
              ),
            ),
            //child: BackgroundLettersWidget(),
          ),
        ],
      ),
    );
  }
}

class LoginFormWidget extends StatelessWidget {
  const LoginFormWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SigninformBloc, SigninformState>(
      listener: (context, state) {
        state.authFailureOrSuccessOption.fold(
          () {},
          (either) => either.fold(
            (f) => f.maybeMap(
              canceledByUser: (_) =>
                  DisplayMessage.showErrorMessage(context, "Canceled by user"),
              serverError: (_) =>
                  DisplayMessage.showErrorMessage(context, "Server error"),
              notAllowed: (_) => DisplayMessage.showErrorMessage(
                  context, "Not Allowed to login"),
              invalidEmailPasswordCombination: (_) =>
                  DisplayMessage.showErrorMessage(
                      context, "Email or Password is wrong"),
              notAnAdmin: (_) => DisplayMessage.showErrorMessage(
                  context, "You are not authorised to login"),
              userNotFound: (_) => DisplayMessage.showErrorMessage(
                  context, "User with given email id is not found"),
              invalidEmail: (_) =>
                  DisplayMessage.showErrorMessage(context, "Email id is wrong"),
              invalidEmailOrPasswordValue: (_) {
                DisplayMessage.showErrorMessage(
                    context, "Email id is wrong or Password is invalid");
              },
              orElse: () {
                DisplayMessage.showErrorMessage(
                    context, "Unknown Error! ${f.runtimeType}");
              },
            ),
            (r) {
              // debugPrint("Need to navigate to Home page");
              // context
              //     .read<AuthBloc>()
              //     .add(const AuthEvent.authCheckRequested());
              ExtendedNavigator.of(context)
                  .pushAndRemoveUntil(Routes.landingPage, (route) => false);
            },
          ),
        );
      },
      builder: (context, state) {
        return Stack(
          children: [
            Form(
              autovalidate: state.showErrorMessages,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "admin panel",
                    style: text100.copyWith(fontWeight: FontWeight.w700),
                  ),
                  verticalSpaceMedium20,
                  SizedBox(
                    width: 500.w,
                    child: TextFormField(
                      cursorColor: AppColors.getPrimaryColor(),
                      style: text30,
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: AppColors.getPrimaryColor()),
                        ),
                        hintText: 'email',
                        hintStyle: text30,
                      ),
                      autocorrect: false,
                      onChanged: (v) => context
                          .read<SigninformBloc>()
                          .add(SigninformEvent.emailChanged(v)),
                      validator: (_) => context
                          .read<SigninformBloc>()
                          .state
                          .emailAddress
                          .value
                          .fold(
                            (f) => f.maybeMap(
                                invalidEmailAdress: (_) => "Email is not valid",
                                orElse: () => null),
                            (_) => null,
                          ),
                    ),
                  ),
                  verticalSpaceMedium20,
                  SizedBox(
                    width: 500.w,
                    child: TextFormField(
                      cursorColor: AppColors.getPrimaryColor(),
                      style: text30,
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: AppColors.getPrimaryColor()),
                        ),
                        hintText: 'password',
                        hintStyle: text30,
                        suffixIcon: IconButton(
                          icon: Icon(
                            state.showPassword
                                ? Icons.visibility_off
                                : Icons.visibility,
                            color: AppColors.getPrimaryColor(),
                          ),
                          onPressed: () {
                            // debugPrint(
                            //     "showPwd is clicked ${context.bloc<SigninformBloc>().state.showPassword}");
                            context.read<SigninformBloc>().add(
                                const SigninformEvent.showPasswordClicked());
                          },
                        ),
                      ),
                      autocorrect: false,
                      obscureText:
                          !context.read<SigninformBloc>().state.showPassword,
                      onChanged: (v) => context
                          .read<SigninformBloc>()
                          .add(SigninformEvent.passwordChanged(v)),
                      validator: (_) => state.password.value.fold(
                        (f) => f.maybeMap(
                          shortPassword: (_) => "Password is too short",
                          orElse: () => null,
                        ),
                        (_) => null,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              bottom: 30.h,
              right: 30.w,
              child: InkWell(
                onTap: state.isLoging
                    ? null
                    : () {
                        context
                            .read<SigninformBloc>()
                            .add(const SigninformEvent.loginPressed());
                      },
                child: state.isLoging
                    ? Container(
                        height: 80,
                        width: 80,
                        child: CircularProgressIndicator(
                          backgroundColor:
                              AppColors.getPrimaryColor().withOpacity(0.3),
                          valueColor: AlwaysStoppedAnimation<Color>(
                              AppColors.getPrimaryColor()),
                          strokeWidth: 3,
                        ),
                      )
                    : Icon(
                        Icons.arrow_forward_ios_sharp,
                        color: AppColors.getPrimaryColor(),
                        size: 80,
                      ).showCursorOnHover.moveRightOnHover,
              ),
            )
          ],
        );
      },
    );
  }
}
