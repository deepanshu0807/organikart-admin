import 'package:organikart_admin/application/content_form_bloc/content_form_bloc.dart';
import 'package:organikart_admin/application/category_form_bloc/category_form_bloc.dart';
import 'package:organikart_admin/routes/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

import 'application/auth/auth_watcher_bloc/auth_watcher_bloc.dart';
import 'application/auth/signinform/signinform_bloc.dart';
import 'config/di/injection.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  configureInjection(Environment.prod);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        ScreenUtil.init(
          constraints,
          designSize: const Size(1920, 1080),
        );
        return MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (_) => getIt<AuthWatcherBloc>()
                ..add(
                  const AuthWatcherEvent.authCheckRequested(),
                ),
            ),
            BlocProvider(create: (_) => getIt<SigninformBloc>()),
            BlocProvider(create: (_) => getIt<CategoryFormBloc>()),
            BlocProvider(create: (_) => getIt<ContentFormBloc>()),
            BlocProvider(
                create: (_) => getIt<CategoryWatcherBloc>()
                  ..add(CategoryWatcherEvent.getCategories())),
            BlocProvider(
                create: (_) => getIt<ContentWatcherBloc>()
                  ..add(ContentWatcherEvent.getContent())),
          ],
          child: MaterialApp(
            title: 'Organikart Admin',
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primaryColor: AppColors.getPrimaryColor(),
              accentColor: AppColors.getSecondaryColor(),
              splashColor: AppColors.getPrimaryColor().withOpacity(0.6),
            ),
            builder: (context, _) {
              return ExtendedNavigator<AppRouter>(
                router: AppRouter(),
              );
            },
          ),
        );
      },
    );
  }
}
