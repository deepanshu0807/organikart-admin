part of 'content_form_bloc.dart';

@freezed
abstract class ContentFormState with _$ContentFormState {
  const factory ContentFormState({
    @required Content content,
    @required bool isLoadingMainContentImage,
    @required bool isSavingContent,
    @required Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
    @required Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
    @required
        Option<Either<InfraFailure, UploadResult>>
            mainContentImageUploadFailureOrSuccessOption,
  }) = _ContentFormState;

  factory ContentFormState.initial() => ContentFormState(
        content: Content(
          id: UniqueId(),
          description: '',
          name: '',
          picUrl: '',
          mrp: Price(''),
          discountPercentage: Percentage(''),
          discountedMrp: Price(''),
          qtyUnit: const QtyUnit.kg(),
          category: Category(
            id: UniqueId(),
            title: '',
            titlePicUrl: '',
          ),
        ),
        isLoadingMainContentImage: false,
        isSavingContent: false,
        saveFailureOrSuccessOption: none(),
        deleteFailureOrSuccessOption: none(),
        mainContentImageUploadFailureOrSuccessOption: none(),
      );
}
