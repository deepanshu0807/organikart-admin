part of 'content_form_bloc.dart';

@freezed
abstract class ContentFormEvent with _$ContentFormEvent {
  const factory ContentFormEvent.selectedCategory(Category category) =
      CategorySelected;

  const factory ContentFormEvent.nameChanged(
    String name,
  ) = _EvNameChanged;

  const factory ContentFormEvent.mrpChanged(
    String mrp,
  ) = _EvMrpChanged;
  const factory ContentFormEvent.discountMrpChanged(
    String discountMrp,
  ) = _EvDiscountMrpChanged;

  const factory ContentFormEvent.descriptionChanged(
    String description,
  ) = _EvDescriptionChanged;

  const factory ContentFormEvent.qtyUnitChanged(
    QtyUnit qtyUnit,
  ) = _EvQtyUnitChanged;

  const factory ContentFormEvent.calculateDiscountPercentage() =
      _EvCalculateDiscountPercentage;

  const factory ContentFormEvent.picUrlChanged(String picUrl) =
      _EvPicUrlChanged;

  const factory ContentFormEvent.uploadImageClicked() = _EvUploadImageClicked;

  const factory ContentFormEvent.initialize() = _EvInitialize;

  const factory ContentFormEvent.saveIsClicked() = _EvSaveIsClicked;

  const factory ContentFormEvent.deleteIsClicked(Content content) =
      _EveDeleteIsClicked;

  const factory ContentFormEvent.selectedForEditing(Content content) =
      _EveSelectedForEditing;
}
