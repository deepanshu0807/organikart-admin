// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'content_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ContentFormEventTearOff {
  const _$ContentFormEventTearOff();

// ignore: unused_element
  CategorySelected selectedCategory(Category category) {
    return CategorySelected(
      category,
    );
  }

// ignore: unused_element
  _EvNameChanged nameChanged(String name) {
    return _EvNameChanged(
      name,
    );
  }

// ignore: unused_element
  _EvMrpChanged mrpChanged(String mrp) {
    return _EvMrpChanged(
      mrp,
    );
  }

// ignore: unused_element
  _EvDiscountMrpChanged discountMrpChanged(String discountMrp) {
    return _EvDiscountMrpChanged(
      discountMrp,
    );
  }

// ignore: unused_element
  _EvDescriptionChanged descriptionChanged(String description) {
    return _EvDescriptionChanged(
      description,
    );
  }

// ignore: unused_element
  _EvQtyUnitChanged qtyUnitChanged(QtyUnit qtyUnit) {
    return _EvQtyUnitChanged(
      qtyUnit,
    );
  }

// ignore: unused_element
  _EvCalculateDiscountPercentage calculateDiscountPercentage() {
    return const _EvCalculateDiscountPercentage();
  }

// ignore: unused_element
  _EvPicUrlChanged picUrlChanged(String picUrl) {
    return _EvPicUrlChanged(
      picUrl,
    );
  }

// ignore: unused_element
  _EvUploadImageClicked uploadImageClicked() {
    return const _EvUploadImageClicked();
  }

// ignore: unused_element
  _EvInitialize initialize() {
    return const _EvInitialize();
  }

// ignore: unused_element
  _EvSaveIsClicked saveIsClicked() {
    return const _EvSaveIsClicked();
  }

// ignore: unused_element
  _EveDeleteIsClicked deleteIsClicked(Content content) {
    return _EveDeleteIsClicked(
      content,
    );
  }

// ignore: unused_element
  _EveSelectedForEditing selectedForEditing(Content content) {
    return _EveSelectedForEditing(
      content,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ContentFormEvent = _$ContentFormEventTearOff();

/// @nodoc
mixin _$ContentFormEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  });
}

/// @nodoc
abstract class $ContentFormEventCopyWith<$Res> {
  factory $ContentFormEventCopyWith(
          ContentFormEvent value, $Res Function(ContentFormEvent) then) =
      _$ContentFormEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ContentFormEventCopyWithImpl<$Res>
    implements $ContentFormEventCopyWith<$Res> {
  _$ContentFormEventCopyWithImpl(this._value, this._then);

  final ContentFormEvent _value;
  // ignore: unused_field
  final $Res Function(ContentFormEvent) _then;
}

/// @nodoc
abstract class $CategorySelectedCopyWith<$Res> {
  factory $CategorySelectedCopyWith(
          CategorySelected value, $Res Function(CategorySelected) then) =
      _$CategorySelectedCopyWithImpl<$Res>;
  $Res call({Category category});

  $CategoryCopyWith<$Res> get category;
}

/// @nodoc
class _$CategorySelectedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements $CategorySelectedCopyWith<$Res> {
  _$CategorySelectedCopyWithImpl(
      CategorySelected _value, $Res Function(CategorySelected) _then)
      : super(_value, (v) => _then(v as CategorySelected));

  @override
  CategorySelected get _value => super._value as CategorySelected;

  @override
  $Res call({
    Object category = freezed,
  }) {
    return _then(CategorySelected(
      category == freezed ? _value.category : category as Category,
    ));
  }

  @override
  $CategoryCopyWith<$Res> get category {
    if (_value.category == null) {
      return null;
    }
    return $CategoryCopyWith<$Res>(_value.category, (value) {
      return _then(_value.copyWith(category: value));
    });
  }
}

/// @nodoc
class _$CategorySelected implements CategorySelected {
  const _$CategorySelected(this.category) : assert(category != null);

  @override
  final Category category;

  @override
  String toString() {
    return 'ContentFormEvent.selectedCategory(category: $category)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is CategorySelected &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(category);

  @override
  $CategorySelectedCopyWith<CategorySelected> get copyWith =>
      _$CategorySelectedCopyWithImpl<CategorySelected>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return selectedCategory(category);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectedCategory != null) {
      return selectedCategory(category);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return selectedCategory(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectedCategory != null) {
      return selectedCategory(this);
    }
    return orElse();
  }
}

abstract class CategorySelected implements ContentFormEvent {
  const factory CategorySelected(Category category) = _$CategorySelected;

  Category get category;
  $CategorySelectedCopyWith<CategorySelected> get copyWith;
}

/// @nodoc
abstract class _$EvNameChangedCopyWith<$Res> {
  factory _$EvNameChangedCopyWith(
          _EvNameChanged value, $Res Function(_EvNameChanged) then) =
      __$EvNameChangedCopyWithImpl<$Res>;
  $Res call({String name});
}

/// @nodoc
class __$EvNameChangedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvNameChangedCopyWith<$Res> {
  __$EvNameChangedCopyWithImpl(
      _EvNameChanged _value, $Res Function(_EvNameChanged) _then)
      : super(_value, (v) => _then(v as _EvNameChanged));

  @override
  _EvNameChanged get _value => super._value as _EvNameChanged;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(_EvNameChanged(
      name == freezed ? _value.name : name as String,
    ));
  }
}

/// @nodoc
class _$_EvNameChanged implements _EvNameChanged {
  const _$_EvNameChanged(this.name) : assert(name != null);

  @override
  final String name;

  @override
  String toString() {
    return 'ContentFormEvent.nameChanged(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EvNameChanged &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @override
  _$EvNameChangedCopyWith<_EvNameChanged> get copyWith =>
      __$EvNameChangedCopyWithImpl<_EvNameChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return nameChanged(name);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (nameChanged != null) {
      return nameChanged(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return nameChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (nameChanged != null) {
      return nameChanged(this);
    }
    return orElse();
  }
}

abstract class _EvNameChanged implements ContentFormEvent {
  const factory _EvNameChanged(String name) = _$_EvNameChanged;

  String get name;
  _$EvNameChangedCopyWith<_EvNameChanged> get copyWith;
}

/// @nodoc
abstract class _$EvMrpChangedCopyWith<$Res> {
  factory _$EvMrpChangedCopyWith(
          _EvMrpChanged value, $Res Function(_EvMrpChanged) then) =
      __$EvMrpChangedCopyWithImpl<$Res>;
  $Res call({String mrp});
}

/// @nodoc
class __$EvMrpChangedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvMrpChangedCopyWith<$Res> {
  __$EvMrpChangedCopyWithImpl(
      _EvMrpChanged _value, $Res Function(_EvMrpChanged) _then)
      : super(_value, (v) => _then(v as _EvMrpChanged));

  @override
  _EvMrpChanged get _value => super._value as _EvMrpChanged;

  @override
  $Res call({
    Object mrp = freezed,
  }) {
    return _then(_EvMrpChanged(
      mrp == freezed ? _value.mrp : mrp as String,
    ));
  }
}

/// @nodoc
class _$_EvMrpChanged implements _EvMrpChanged {
  const _$_EvMrpChanged(this.mrp) : assert(mrp != null);

  @override
  final String mrp;

  @override
  String toString() {
    return 'ContentFormEvent.mrpChanged(mrp: $mrp)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EvMrpChanged &&
            (identical(other.mrp, mrp) ||
                const DeepCollectionEquality().equals(other.mrp, mrp)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(mrp);

  @override
  _$EvMrpChangedCopyWith<_EvMrpChanged> get copyWith =>
      __$EvMrpChangedCopyWithImpl<_EvMrpChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return mrpChanged(mrp);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (mrpChanged != null) {
      return mrpChanged(mrp);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return mrpChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (mrpChanged != null) {
      return mrpChanged(this);
    }
    return orElse();
  }
}

abstract class _EvMrpChanged implements ContentFormEvent {
  const factory _EvMrpChanged(String mrp) = _$_EvMrpChanged;

  String get mrp;
  _$EvMrpChangedCopyWith<_EvMrpChanged> get copyWith;
}

/// @nodoc
abstract class _$EvDiscountMrpChangedCopyWith<$Res> {
  factory _$EvDiscountMrpChangedCopyWith(_EvDiscountMrpChanged value,
          $Res Function(_EvDiscountMrpChanged) then) =
      __$EvDiscountMrpChangedCopyWithImpl<$Res>;
  $Res call({String discountMrp});
}

/// @nodoc
class __$EvDiscountMrpChangedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvDiscountMrpChangedCopyWith<$Res> {
  __$EvDiscountMrpChangedCopyWithImpl(
      _EvDiscountMrpChanged _value, $Res Function(_EvDiscountMrpChanged) _then)
      : super(_value, (v) => _then(v as _EvDiscountMrpChanged));

  @override
  _EvDiscountMrpChanged get _value => super._value as _EvDiscountMrpChanged;

  @override
  $Res call({
    Object discountMrp = freezed,
  }) {
    return _then(_EvDiscountMrpChanged(
      discountMrp == freezed ? _value.discountMrp : discountMrp as String,
    ));
  }
}

/// @nodoc
class _$_EvDiscountMrpChanged implements _EvDiscountMrpChanged {
  const _$_EvDiscountMrpChanged(this.discountMrp) : assert(discountMrp != null);

  @override
  final String discountMrp;

  @override
  String toString() {
    return 'ContentFormEvent.discountMrpChanged(discountMrp: $discountMrp)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EvDiscountMrpChanged &&
            (identical(other.discountMrp, discountMrp) ||
                const DeepCollectionEquality()
                    .equals(other.discountMrp, discountMrp)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(discountMrp);

  @override
  _$EvDiscountMrpChangedCopyWith<_EvDiscountMrpChanged> get copyWith =>
      __$EvDiscountMrpChangedCopyWithImpl<_EvDiscountMrpChanged>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return discountMrpChanged(discountMrp);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (discountMrpChanged != null) {
      return discountMrpChanged(discountMrp);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return discountMrpChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (discountMrpChanged != null) {
      return discountMrpChanged(this);
    }
    return orElse();
  }
}

abstract class _EvDiscountMrpChanged implements ContentFormEvent {
  const factory _EvDiscountMrpChanged(String discountMrp) =
      _$_EvDiscountMrpChanged;

  String get discountMrp;
  _$EvDiscountMrpChangedCopyWith<_EvDiscountMrpChanged> get copyWith;
}

/// @nodoc
abstract class _$EvDescriptionChangedCopyWith<$Res> {
  factory _$EvDescriptionChangedCopyWith(_EvDescriptionChanged value,
          $Res Function(_EvDescriptionChanged) then) =
      __$EvDescriptionChangedCopyWithImpl<$Res>;
  $Res call({String description});
}

/// @nodoc
class __$EvDescriptionChangedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvDescriptionChangedCopyWith<$Res> {
  __$EvDescriptionChangedCopyWithImpl(
      _EvDescriptionChanged _value, $Res Function(_EvDescriptionChanged) _then)
      : super(_value, (v) => _then(v as _EvDescriptionChanged));

  @override
  _EvDescriptionChanged get _value => super._value as _EvDescriptionChanged;

  @override
  $Res call({
    Object description = freezed,
  }) {
    return _then(_EvDescriptionChanged(
      description == freezed ? _value.description : description as String,
    ));
  }
}

/// @nodoc
class _$_EvDescriptionChanged implements _EvDescriptionChanged {
  const _$_EvDescriptionChanged(this.description) : assert(description != null);

  @override
  final String description;

  @override
  String toString() {
    return 'ContentFormEvent.descriptionChanged(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EvDescriptionChanged &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(description);

  @override
  _$EvDescriptionChangedCopyWith<_EvDescriptionChanged> get copyWith =>
      __$EvDescriptionChangedCopyWithImpl<_EvDescriptionChanged>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return descriptionChanged(description);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (descriptionChanged != null) {
      return descriptionChanged(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return descriptionChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (descriptionChanged != null) {
      return descriptionChanged(this);
    }
    return orElse();
  }
}

abstract class _EvDescriptionChanged implements ContentFormEvent {
  const factory _EvDescriptionChanged(String description) =
      _$_EvDescriptionChanged;

  String get description;
  _$EvDescriptionChangedCopyWith<_EvDescriptionChanged> get copyWith;
}

/// @nodoc
abstract class _$EvQtyUnitChangedCopyWith<$Res> {
  factory _$EvQtyUnitChangedCopyWith(
          _EvQtyUnitChanged value, $Res Function(_EvQtyUnitChanged) then) =
      __$EvQtyUnitChangedCopyWithImpl<$Res>;
  $Res call({QtyUnit qtyUnit});

  $QtyUnitCopyWith<$Res> get qtyUnit;
}

/// @nodoc
class __$EvQtyUnitChangedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvQtyUnitChangedCopyWith<$Res> {
  __$EvQtyUnitChangedCopyWithImpl(
      _EvQtyUnitChanged _value, $Res Function(_EvQtyUnitChanged) _then)
      : super(_value, (v) => _then(v as _EvQtyUnitChanged));

  @override
  _EvQtyUnitChanged get _value => super._value as _EvQtyUnitChanged;

  @override
  $Res call({
    Object qtyUnit = freezed,
  }) {
    return _then(_EvQtyUnitChanged(
      qtyUnit == freezed ? _value.qtyUnit : qtyUnit as QtyUnit,
    ));
  }

  @override
  $QtyUnitCopyWith<$Res> get qtyUnit {
    if (_value.qtyUnit == null) {
      return null;
    }
    return $QtyUnitCopyWith<$Res>(_value.qtyUnit, (value) {
      return _then(_value.copyWith(qtyUnit: value));
    });
  }
}

/// @nodoc
class _$_EvQtyUnitChanged implements _EvQtyUnitChanged {
  const _$_EvQtyUnitChanged(this.qtyUnit) : assert(qtyUnit != null);

  @override
  final QtyUnit qtyUnit;

  @override
  String toString() {
    return 'ContentFormEvent.qtyUnitChanged(qtyUnit: $qtyUnit)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EvQtyUnitChanged &&
            (identical(other.qtyUnit, qtyUnit) ||
                const DeepCollectionEquality().equals(other.qtyUnit, qtyUnit)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(qtyUnit);

  @override
  _$EvQtyUnitChangedCopyWith<_EvQtyUnitChanged> get copyWith =>
      __$EvQtyUnitChangedCopyWithImpl<_EvQtyUnitChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return qtyUnitChanged(qtyUnit);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (qtyUnitChanged != null) {
      return qtyUnitChanged(qtyUnit);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return qtyUnitChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (qtyUnitChanged != null) {
      return qtyUnitChanged(this);
    }
    return orElse();
  }
}

abstract class _EvQtyUnitChanged implements ContentFormEvent {
  const factory _EvQtyUnitChanged(QtyUnit qtyUnit) = _$_EvQtyUnitChanged;

  QtyUnit get qtyUnit;
  _$EvQtyUnitChangedCopyWith<_EvQtyUnitChanged> get copyWith;
}

/// @nodoc
abstract class _$EvCalculateDiscountPercentageCopyWith<$Res> {
  factory _$EvCalculateDiscountPercentageCopyWith(
          _EvCalculateDiscountPercentage value,
          $Res Function(_EvCalculateDiscountPercentage) then) =
      __$EvCalculateDiscountPercentageCopyWithImpl<$Res>;
}

/// @nodoc
class __$EvCalculateDiscountPercentageCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvCalculateDiscountPercentageCopyWith<$Res> {
  __$EvCalculateDiscountPercentageCopyWithImpl(
      _EvCalculateDiscountPercentage _value,
      $Res Function(_EvCalculateDiscountPercentage) _then)
      : super(_value, (v) => _then(v as _EvCalculateDiscountPercentage));

  @override
  _EvCalculateDiscountPercentage get _value =>
      super._value as _EvCalculateDiscountPercentage;
}

/// @nodoc
class _$_EvCalculateDiscountPercentage
    implements _EvCalculateDiscountPercentage {
  const _$_EvCalculateDiscountPercentage();

  @override
  String toString() {
    return 'ContentFormEvent.calculateDiscountPercentage()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _EvCalculateDiscountPercentage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return calculateDiscountPercentage();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (calculateDiscountPercentage != null) {
      return calculateDiscountPercentage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return calculateDiscountPercentage(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (calculateDiscountPercentage != null) {
      return calculateDiscountPercentage(this);
    }
    return orElse();
  }
}

abstract class _EvCalculateDiscountPercentage implements ContentFormEvent {
  const factory _EvCalculateDiscountPercentage() =
      _$_EvCalculateDiscountPercentage;
}

/// @nodoc
abstract class _$EvPicUrlChangedCopyWith<$Res> {
  factory _$EvPicUrlChangedCopyWith(
          _EvPicUrlChanged value, $Res Function(_EvPicUrlChanged) then) =
      __$EvPicUrlChangedCopyWithImpl<$Res>;
  $Res call({String picUrl});
}

/// @nodoc
class __$EvPicUrlChangedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvPicUrlChangedCopyWith<$Res> {
  __$EvPicUrlChangedCopyWithImpl(
      _EvPicUrlChanged _value, $Res Function(_EvPicUrlChanged) _then)
      : super(_value, (v) => _then(v as _EvPicUrlChanged));

  @override
  _EvPicUrlChanged get _value => super._value as _EvPicUrlChanged;

  @override
  $Res call({
    Object picUrl = freezed,
  }) {
    return _then(_EvPicUrlChanged(
      picUrl == freezed ? _value.picUrl : picUrl as String,
    ));
  }
}

/// @nodoc
class _$_EvPicUrlChanged implements _EvPicUrlChanged {
  const _$_EvPicUrlChanged(this.picUrl) : assert(picUrl != null);

  @override
  final String picUrl;

  @override
  String toString() {
    return 'ContentFormEvent.picUrlChanged(picUrl: $picUrl)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EvPicUrlChanged &&
            (identical(other.picUrl, picUrl) ||
                const DeepCollectionEquality().equals(other.picUrl, picUrl)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(picUrl);

  @override
  _$EvPicUrlChangedCopyWith<_EvPicUrlChanged> get copyWith =>
      __$EvPicUrlChangedCopyWithImpl<_EvPicUrlChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return picUrlChanged(picUrl);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (picUrlChanged != null) {
      return picUrlChanged(picUrl);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return picUrlChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (picUrlChanged != null) {
      return picUrlChanged(this);
    }
    return orElse();
  }
}

abstract class _EvPicUrlChanged implements ContentFormEvent {
  const factory _EvPicUrlChanged(String picUrl) = _$_EvPicUrlChanged;

  String get picUrl;
  _$EvPicUrlChangedCopyWith<_EvPicUrlChanged> get copyWith;
}

/// @nodoc
abstract class _$EvUploadImageClickedCopyWith<$Res> {
  factory _$EvUploadImageClickedCopyWith(_EvUploadImageClicked value,
          $Res Function(_EvUploadImageClicked) then) =
      __$EvUploadImageClickedCopyWithImpl<$Res>;
}

/// @nodoc
class __$EvUploadImageClickedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvUploadImageClickedCopyWith<$Res> {
  __$EvUploadImageClickedCopyWithImpl(
      _EvUploadImageClicked _value, $Res Function(_EvUploadImageClicked) _then)
      : super(_value, (v) => _then(v as _EvUploadImageClicked));

  @override
  _EvUploadImageClicked get _value => super._value as _EvUploadImageClicked;
}

/// @nodoc
class _$_EvUploadImageClicked implements _EvUploadImageClicked {
  const _$_EvUploadImageClicked();

  @override
  String toString() {
    return 'ContentFormEvent.uploadImageClicked()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _EvUploadImageClicked);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return uploadImageClicked();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (uploadImageClicked != null) {
      return uploadImageClicked();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return uploadImageClicked(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (uploadImageClicked != null) {
      return uploadImageClicked(this);
    }
    return orElse();
  }
}

abstract class _EvUploadImageClicked implements ContentFormEvent {
  const factory _EvUploadImageClicked() = _$_EvUploadImageClicked;
}

/// @nodoc
abstract class _$EvInitializeCopyWith<$Res> {
  factory _$EvInitializeCopyWith(
          _EvInitialize value, $Res Function(_EvInitialize) then) =
      __$EvInitializeCopyWithImpl<$Res>;
}

/// @nodoc
class __$EvInitializeCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvInitializeCopyWith<$Res> {
  __$EvInitializeCopyWithImpl(
      _EvInitialize _value, $Res Function(_EvInitialize) _then)
      : super(_value, (v) => _then(v as _EvInitialize));

  @override
  _EvInitialize get _value => super._value as _EvInitialize;
}

/// @nodoc
class _$_EvInitialize implements _EvInitialize {
  const _$_EvInitialize();

  @override
  String toString() {
    return 'ContentFormEvent.initialize()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _EvInitialize);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return initialize();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialize != null) {
      return initialize();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return initialize(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialize != null) {
      return initialize(this);
    }
    return orElse();
  }
}

abstract class _EvInitialize implements ContentFormEvent {
  const factory _EvInitialize() = _$_EvInitialize;
}

/// @nodoc
abstract class _$EvSaveIsClickedCopyWith<$Res> {
  factory _$EvSaveIsClickedCopyWith(
          _EvSaveIsClicked value, $Res Function(_EvSaveIsClicked) then) =
      __$EvSaveIsClickedCopyWithImpl<$Res>;
}

/// @nodoc
class __$EvSaveIsClickedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EvSaveIsClickedCopyWith<$Res> {
  __$EvSaveIsClickedCopyWithImpl(
      _EvSaveIsClicked _value, $Res Function(_EvSaveIsClicked) _then)
      : super(_value, (v) => _then(v as _EvSaveIsClicked));

  @override
  _EvSaveIsClicked get _value => super._value as _EvSaveIsClicked;
}

/// @nodoc
class _$_EvSaveIsClicked implements _EvSaveIsClicked {
  const _$_EvSaveIsClicked();

  @override
  String toString() {
    return 'ContentFormEvent.saveIsClicked()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _EvSaveIsClicked);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return saveIsClicked();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (saveIsClicked != null) {
      return saveIsClicked();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return saveIsClicked(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (saveIsClicked != null) {
      return saveIsClicked(this);
    }
    return orElse();
  }
}

abstract class _EvSaveIsClicked implements ContentFormEvent {
  const factory _EvSaveIsClicked() = _$_EvSaveIsClicked;
}

/// @nodoc
abstract class _$EveDeleteIsClickedCopyWith<$Res> {
  factory _$EveDeleteIsClickedCopyWith(
          _EveDeleteIsClicked value, $Res Function(_EveDeleteIsClicked) then) =
      __$EveDeleteIsClickedCopyWithImpl<$Res>;
  $Res call({Content content});

  $ContentCopyWith<$Res> get content;
}

/// @nodoc
class __$EveDeleteIsClickedCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EveDeleteIsClickedCopyWith<$Res> {
  __$EveDeleteIsClickedCopyWithImpl(
      _EveDeleteIsClicked _value, $Res Function(_EveDeleteIsClicked) _then)
      : super(_value, (v) => _then(v as _EveDeleteIsClicked));

  @override
  _EveDeleteIsClicked get _value => super._value as _EveDeleteIsClicked;

  @override
  $Res call({
    Object content = freezed,
  }) {
    return _then(_EveDeleteIsClicked(
      content == freezed ? _value.content : content as Content,
    ));
  }

  @override
  $ContentCopyWith<$Res> get content {
    if (_value.content == null) {
      return null;
    }
    return $ContentCopyWith<$Res>(_value.content, (value) {
      return _then(_value.copyWith(content: value));
    });
  }
}

/// @nodoc
class _$_EveDeleteIsClicked implements _EveDeleteIsClicked {
  const _$_EveDeleteIsClicked(this.content) : assert(content != null);

  @override
  final Content content;

  @override
  String toString() {
    return 'ContentFormEvent.deleteIsClicked(content: $content)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EveDeleteIsClicked &&
            (identical(other.content, content) ||
                const DeepCollectionEquality().equals(other.content, content)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(content);

  @override
  _$EveDeleteIsClickedCopyWith<_EveDeleteIsClicked> get copyWith =>
      __$EveDeleteIsClickedCopyWithImpl<_EveDeleteIsClicked>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return deleteIsClicked(content);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteIsClicked != null) {
      return deleteIsClicked(content);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return deleteIsClicked(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteIsClicked != null) {
      return deleteIsClicked(this);
    }
    return orElse();
  }
}

abstract class _EveDeleteIsClicked implements ContentFormEvent {
  const factory _EveDeleteIsClicked(Content content) = _$_EveDeleteIsClicked;

  Content get content;
  _$EveDeleteIsClickedCopyWith<_EveDeleteIsClicked> get copyWith;
}

/// @nodoc
abstract class _$EveSelectedForEditingCopyWith<$Res> {
  factory _$EveSelectedForEditingCopyWith(_EveSelectedForEditing value,
          $Res Function(_EveSelectedForEditing) then) =
      __$EveSelectedForEditingCopyWithImpl<$Res>;
  $Res call({Content content});

  $ContentCopyWith<$Res> get content;
}

/// @nodoc
class __$EveSelectedForEditingCopyWithImpl<$Res>
    extends _$ContentFormEventCopyWithImpl<$Res>
    implements _$EveSelectedForEditingCopyWith<$Res> {
  __$EveSelectedForEditingCopyWithImpl(_EveSelectedForEditing _value,
      $Res Function(_EveSelectedForEditing) _then)
      : super(_value, (v) => _then(v as _EveSelectedForEditing));

  @override
  _EveSelectedForEditing get _value => super._value as _EveSelectedForEditing;

  @override
  $Res call({
    Object content = freezed,
  }) {
    return _then(_EveSelectedForEditing(
      content == freezed ? _value.content : content as Content,
    ));
  }

  @override
  $ContentCopyWith<$Res> get content {
    if (_value.content == null) {
      return null;
    }
    return $ContentCopyWith<$Res>(_value.content, (value) {
      return _then(_value.copyWith(content: value));
    });
  }
}

/// @nodoc
class _$_EveSelectedForEditing implements _EveSelectedForEditing {
  const _$_EveSelectedForEditing(this.content) : assert(content != null);

  @override
  final Content content;

  @override
  String toString() {
    return 'ContentFormEvent.selectedForEditing(content: $content)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EveSelectedForEditing &&
            (identical(other.content, content) ||
                const DeepCollectionEquality().equals(other.content, content)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(content);

  @override
  _$EveSelectedForEditingCopyWith<_EveSelectedForEditing> get copyWith =>
      __$EveSelectedForEditingCopyWithImpl<_EveSelectedForEditing>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectedCategory(Category category),
    @required Result nameChanged(String name),
    @required Result mrpChanged(String mrp),
    @required Result discountMrpChanged(String discountMrp),
    @required Result descriptionChanged(String description),
    @required Result qtyUnitChanged(QtyUnit qtyUnit),
    @required Result calculateDiscountPercentage(),
    @required Result picUrlChanged(String picUrl),
    @required Result uploadImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Content content),
    @required Result selectedForEditing(Content content),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return selectedForEditing(content);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectedCategory(Category category),
    Result nameChanged(String name),
    Result mrpChanged(String mrp),
    Result discountMrpChanged(String discountMrp),
    Result descriptionChanged(String description),
    Result qtyUnitChanged(QtyUnit qtyUnit),
    Result calculateDiscountPercentage(),
    Result picUrlChanged(String picUrl),
    Result uploadImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Content content),
    Result selectedForEditing(Content content),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectedForEditing != null) {
      return selectedForEditing(content);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectedCategory(CategorySelected value),
    @required Result nameChanged(_EvNameChanged value),
    @required Result mrpChanged(_EvMrpChanged value),
    @required Result discountMrpChanged(_EvDiscountMrpChanged value),
    @required Result descriptionChanged(_EvDescriptionChanged value),
    @required Result qtyUnitChanged(_EvQtyUnitChanged value),
    @required
        Result calculateDiscountPercentage(
            _EvCalculateDiscountPercentage value),
    @required Result picUrlChanged(_EvPicUrlChanged value),
    @required Result uploadImageClicked(_EvUploadImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(selectedCategory != null);
    assert(nameChanged != null);
    assert(mrpChanged != null);
    assert(discountMrpChanged != null);
    assert(descriptionChanged != null);
    assert(qtyUnitChanged != null);
    assert(calculateDiscountPercentage != null);
    assert(picUrlChanged != null);
    assert(uploadImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return selectedForEditing(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectedCategory(CategorySelected value),
    Result nameChanged(_EvNameChanged value),
    Result mrpChanged(_EvMrpChanged value),
    Result discountMrpChanged(_EvDiscountMrpChanged value),
    Result descriptionChanged(_EvDescriptionChanged value),
    Result qtyUnitChanged(_EvQtyUnitChanged value),
    Result calculateDiscountPercentage(_EvCalculateDiscountPercentage value),
    Result picUrlChanged(_EvPicUrlChanged value),
    Result uploadImageClicked(_EvUploadImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectedForEditing != null) {
      return selectedForEditing(this);
    }
    return orElse();
  }
}

abstract class _EveSelectedForEditing implements ContentFormEvent {
  const factory _EveSelectedForEditing(Content content) =
      _$_EveSelectedForEditing;

  Content get content;
  _$EveSelectedForEditingCopyWith<_EveSelectedForEditing> get copyWith;
}

/// @nodoc
class _$ContentFormStateTearOff {
  const _$ContentFormStateTearOff();

// ignore: unused_element
  _ContentFormState call(
      {@required
          Content content,
      @required
          bool isLoadingMainContentImage,
      @required
          bool isSavingContent,
      @required
          Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      @required
          Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
      @required
          Option<Either<InfraFailure, UploadResult>>
              mainContentImageUploadFailureOrSuccessOption}) {
    return _ContentFormState(
      content: content,
      isLoadingMainContentImage: isLoadingMainContentImage,
      isSavingContent: isSavingContent,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption,
      deleteFailureOrSuccessOption: deleteFailureOrSuccessOption,
      mainContentImageUploadFailureOrSuccessOption:
          mainContentImageUploadFailureOrSuccessOption,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ContentFormState = _$ContentFormStateTearOff();

/// @nodoc
mixin _$ContentFormState {
  Content get content;
  bool get isLoadingMainContentImage;
  bool get isSavingContent;
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;
  Option<Either<InfraFailure, Unit>> get deleteFailureOrSuccessOption;
  Option<Either<InfraFailure, UploadResult>>
      get mainContentImageUploadFailureOrSuccessOption;

  $ContentFormStateCopyWith<ContentFormState> get copyWith;
}

/// @nodoc
abstract class $ContentFormStateCopyWith<$Res> {
  factory $ContentFormStateCopyWith(
          ContentFormState value, $Res Function(ContentFormState) then) =
      _$ContentFormStateCopyWithImpl<$Res>;
  $Res call(
      {Content content,
      bool isLoadingMainContentImage,
      bool isSavingContent,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
      Option<Either<InfraFailure, UploadResult>>
          mainContentImageUploadFailureOrSuccessOption});

  $ContentCopyWith<$Res> get content;
}

/// @nodoc
class _$ContentFormStateCopyWithImpl<$Res>
    implements $ContentFormStateCopyWith<$Res> {
  _$ContentFormStateCopyWithImpl(this._value, this._then);

  final ContentFormState _value;
  // ignore: unused_field
  final $Res Function(ContentFormState) _then;

  @override
  $Res call({
    Object content = freezed,
    Object isLoadingMainContentImage = freezed,
    Object isSavingContent = freezed,
    Object saveFailureOrSuccessOption = freezed,
    Object deleteFailureOrSuccessOption = freezed,
    Object mainContentImageUploadFailureOrSuccessOption = freezed,
  }) {
    return _then(_value.copyWith(
      content: content == freezed ? _value.content : content as Content,
      isLoadingMainContentImage: isLoadingMainContentImage == freezed
          ? _value.isLoadingMainContentImage
          : isLoadingMainContentImage as bool,
      isSavingContent: isSavingContent == freezed
          ? _value.isSavingContent
          : isSavingContent as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      deleteFailureOrSuccessOption: deleteFailureOrSuccessOption == freezed
          ? _value.deleteFailureOrSuccessOption
          : deleteFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      mainContentImageUploadFailureOrSuccessOption:
          mainContentImageUploadFailureOrSuccessOption == freezed
              ? _value.mainContentImageUploadFailureOrSuccessOption
              : mainContentImageUploadFailureOrSuccessOption
                  as Option<Either<InfraFailure, UploadResult>>,
    ));
  }

  @override
  $ContentCopyWith<$Res> get content {
    if (_value.content == null) {
      return null;
    }
    return $ContentCopyWith<$Res>(_value.content, (value) {
      return _then(_value.copyWith(content: value));
    });
  }
}

/// @nodoc
abstract class _$ContentFormStateCopyWith<$Res>
    implements $ContentFormStateCopyWith<$Res> {
  factory _$ContentFormStateCopyWith(
          _ContentFormState value, $Res Function(_ContentFormState) then) =
      __$ContentFormStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Content content,
      bool isLoadingMainContentImage,
      bool isSavingContent,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
      Option<Either<InfraFailure, UploadResult>>
          mainContentImageUploadFailureOrSuccessOption});

  @override
  $ContentCopyWith<$Res> get content;
}

/// @nodoc
class __$ContentFormStateCopyWithImpl<$Res>
    extends _$ContentFormStateCopyWithImpl<$Res>
    implements _$ContentFormStateCopyWith<$Res> {
  __$ContentFormStateCopyWithImpl(
      _ContentFormState _value, $Res Function(_ContentFormState) _then)
      : super(_value, (v) => _then(v as _ContentFormState));

  @override
  _ContentFormState get _value => super._value as _ContentFormState;

  @override
  $Res call({
    Object content = freezed,
    Object isLoadingMainContentImage = freezed,
    Object isSavingContent = freezed,
    Object saveFailureOrSuccessOption = freezed,
    Object deleteFailureOrSuccessOption = freezed,
    Object mainContentImageUploadFailureOrSuccessOption = freezed,
  }) {
    return _then(_ContentFormState(
      content: content == freezed ? _value.content : content as Content,
      isLoadingMainContentImage: isLoadingMainContentImage == freezed
          ? _value.isLoadingMainContentImage
          : isLoadingMainContentImage as bool,
      isSavingContent: isSavingContent == freezed
          ? _value.isSavingContent
          : isSavingContent as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      deleteFailureOrSuccessOption: deleteFailureOrSuccessOption == freezed
          ? _value.deleteFailureOrSuccessOption
          : deleteFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      mainContentImageUploadFailureOrSuccessOption:
          mainContentImageUploadFailureOrSuccessOption == freezed
              ? _value.mainContentImageUploadFailureOrSuccessOption
              : mainContentImageUploadFailureOrSuccessOption
                  as Option<Either<InfraFailure, UploadResult>>,
    ));
  }
}

/// @nodoc
class _$_ContentFormState implements _ContentFormState {
  const _$_ContentFormState(
      {@required this.content,
      @required this.isLoadingMainContentImage,
      @required this.isSavingContent,
      @required this.saveFailureOrSuccessOption,
      @required this.deleteFailureOrSuccessOption,
      @required this.mainContentImageUploadFailureOrSuccessOption})
      : assert(content != null),
        assert(isLoadingMainContentImage != null),
        assert(isSavingContent != null),
        assert(saveFailureOrSuccessOption != null),
        assert(deleteFailureOrSuccessOption != null),
        assert(mainContentImageUploadFailureOrSuccessOption != null);

  @override
  final Content content;
  @override
  final bool isLoadingMainContentImage;
  @override
  final bool isSavingContent;
  @override
  final Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption;
  @override
  final Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption;
  @override
  final Option<Either<InfraFailure, UploadResult>>
      mainContentImageUploadFailureOrSuccessOption;

  @override
  String toString() {
    return 'ContentFormState(content: $content, isLoadingMainContentImage: $isLoadingMainContentImage, isSavingContent: $isSavingContent, saveFailureOrSuccessOption: $saveFailureOrSuccessOption, deleteFailureOrSuccessOption: $deleteFailureOrSuccessOption, mainContentImageUploadFailureOrSuccessOption: $mainContentImageUploadFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ContentFormState &&
            (identical(other.content, content) ||
                const DeepCollectionEquality()
                    .equals(other.content, content)) &&
            (identical(other.isLoadingMainContentImage,
                    isLoadingMainContentImage) ||
                const DeepCollectionEquality().equals(
                    other.isLoadingMainContentImage,
                    isLoadingMainContentImage)) &&
            (identical(other.isSavingContent, isSavingContent) ||
                const DeepCollectionEquality()
                    .equals(other.isSavingContent, isSavingContent)) &&
            (identical(other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption)) &&
            (identical(other.deleteFailureOrSuccessOption,
                    deleteFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.deleteFailureOrSuccessOption,
                    deleteFailureOrSuccessOption)) &&
            (identical(other.mainContentImageUploadFailureOrSuccessOption,
                    mainContentImageUploadFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.mainContentImageUploadFailureOrSuccessOption,
                    mainContentImageUploadFailureOrSuccessOption)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(content) ^
      const DeepCollectionEquality().hash(isLoadingMainContentImage) ^
      const DeepCollectionEquality().hash(isSavingContent) ^
      const DeepCollectionEquality().hash(saveFailureOrSuccessOption) ^
      const DeepCollectionEquality().hash(deleteFailureOrSuccessOption) ^
      const DeepCollectionEquality()
          .hash(mainContentImageUploadFailureOrSuccessOption);

  @override
  _$ContentFormStateCopyWith<_ContentFormState> get copyWith =>
      __$ContentFormStateCopyWithImpl<_ContentFormState>(this, _$identity);
}

abstract class _ContentFormState implements ContentFormState {
  const factory _ContentFormState(
          {@required
              Content content,
          @required
              bool isLoadingMainContentImage,
          @required
              bool isSavingContent,
          @required
              Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
          @required
              Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
          @required
              Option<Either<InfraFailure, UploadResult>>
                  mainContentImageUploadFailureOrSuccessOption}) =
      _$_ContentFormState;

  @override
  Content get content;
  @override
  bool get isLoadingMainContentImage;
  @override
  bool get isSavingContent;
  @override
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;
  @override
  Option<Either<InfraFailure, Unit>> get deleteFailureOrSuccessOption;
  @override
  Option<Either<InfraFailure, UploadResult>>
      get mainContentImageUploadFailureOrSuccessOption;
  @override
  _$ContentFormStateCopyWith<_ContentFormState> get copyWith;
}
