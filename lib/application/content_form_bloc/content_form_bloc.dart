import 'dart:async';
import 'package:dartz/dartz.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'content_form_event.dart';
part 'content_form_state.dart';
part 'content_form_bloc.freezed.dart';

@injectable
class ContentFormBloc extends Bloc<ContentFormEvent, ContentFormState> {
  final IContentRepo _iContentRepo;
  ContentFormBloc(this._iContentRepo) : super(ContentFormState.initial());

  @override
  Stream<ContentFormState> mapEventToState(
    ContentFormEvent event,
  ) async* {
    yield* event.map(
        selectedCategory: _selectedCategory,
        descriptionChanged: _descriptionChanged,
        nameChanged: _nameChanged,
        mrpChanged: _mrpChanged,
        discountMrpChanged: _discountMrpChanged,
        qtyUnitChanged: _qtyUnitChanged,
        calculateDiscountPercentage: _calculateDiscountPercentage,
        picUrlChanged: _picUrlChanged,
        uploadImageClicked: _uploadImageClicked,
        initialize: _initialize,
        saveIsClicked: _saveIsClicked,
        deleteIsClicked: _deleteIsClicked,
        selectedForEditing: _selectedForEditing);
  }

  Stream<ContentFormState> _selectedCategory(CategorySelected value) async* {
    yield state.copyWith(
      content: state.content.copyWith(category: value.category),
      isSavingContent: false,
      isLoadingMainContentImage: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      mainContentImageUploadFailureOrSuccessOption: none(),
    );
  }

  Stream<ContentFormState> _picUrlChanged(_EvPicUrlChanged value) async* {
    yield state.copyWith(
      content: state.content.copyWith(
        picUrl: value.picUrl,
      ),
      isSavingContent: false,
      isLoadingMainContentImage: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      mainContentImageUploadFailureOrSuccessOption: none(),
    );
  }

  Stream<ContentFormState> _uploadImageClicked(
      _EvUploadImageClicked value) async* {
    yield state.copyWith(
      isLoadingMainContentImage: true,
      isSavingContent: true,
    );

    yield state.copyWith(
      isSavingContent: true,
      isLoadingMainContentImage: true,
      saveFailureOrSuccessOption: none(),
      mainContentImageUploadFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
    );
    final result = await _iContentRepo.uploadMainContentImage(state.content);

    yield state.copyWith(
      isLoadingMainContentImage: false,
      isSavingContent: false,
      mainContentImageUploadFailureOrSuccessOption: some(result),
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
    );

    result.fold(
      (l) {},
      (r) {
        add(ContentFormEvent.picUrlChanged(r.picUrl));
      },
    );
  }

  Stream<ContentFormState> _nameChanged(_EvNameChanged value) async* {
    yield state.copyWith(
      content: state.content.copyWith(
        name: value.name,
      ),
      isSavingContent: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      mainContentImageUploadFailureOrSuccessOption: none(),
    );
  }

  Stream<ContentFormState> _qtyUnitChanged(_EvQtyUnitChanged value) async* {
    yield state.copyWith(
      content: state.content.copyWith(
        qtyUnit: value.qtyUnit,
      ),
      isSavingContent: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      mainContentImageUploadFailureOrSuccessOption: none(),
    );
  }

  Stream<ContentFormState> _mrpChanged(_EvMrpChanged value) async* {
    yield state.copyWith(
      content: state.content.copyWith(
        mrp: Price(value.mrp),
      ),
      isSavingContent: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      mainContentImageUploadFailureOrSuccessOption: none(),
    );
    add(_EvCalculateDiscountPercentage());
  }

  Stream<ContentFormState> _discountMrpChanged(
      _EvDiscountMrpChanged value) async* {
    yield state.copyWith(
      content: state.content.copyWith(
        discountedMrp: Price(value.discountMrp),
      ),
      isSavingContent: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      mainContentImageUploadFailureOrSuccessOption: none(),
    );
    add(_EvCalculateDiscountPercentage());
  }

  Stream<ContentFormState> _calculateDiscountPercentage(
      _EvCalculateDiscountPercentage value) async* {
    final actualPrice = state.content.mrp.getOrElse(0);
    final discountedPrice = state.content.discountedMrp.getOrElse(0);

    final discountPercenatge =
        (((actualPrice - discountedPrice) / actualPrice) * 100).toInt();

    yield state.copyWith(
      content: state.content.copyWith(
        discountPercentage: Percentage(discountPercenatge.toString()),
      ),
      isSavingContent: false,
      isLoadingMainContentImage: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      mainContentImageUploadFailureOrSuccessOption: none(),
    );
  }

  Stream<ContentFormState> _descriptionChanged(
      _EvDescriptionChanged value) async* {
    yield state.copyWith(
      content: state.content.copyWith(
        description: value.description,
      ),
      isSavingContent: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      mainContentImageUploadFailureOrSuccessOption: none(),
    );
  }

  Stream<ContentFormState> _initialize(_EvInitialize value) async* {
    yield ContentFormState.initial();
  }

  Stream<ContentFormState> _saveIsClicked(_EvSaveIsClicked value) async* {
    yield state.copyWith(
      isSavingContent: true,
      isLoadingMainContentImage: true,
    );

    final result = await _iContentRepo.create(state.content);

    yield state.copyWith(
      isSavingContent: false,
      isLoadingMainContentImage: false,
      mainContentImageUploadFailureOrSuccessOption: none(),
      saveFailureOrSuccessOption: some(result),
      deleteFailureOrSuccessOption: none(),
    );

    result.fold(
      (l) {},
      (r) {
        add(const ContentFormEvent.initialize());
      },
    );
  }

  Stream<ContentFormState> _deleteIsClicked(_EveDeleteIsClicked value) async* {
    yield state.copyWith(
      isSavingContent: false,
      isLoadingMainContentImage: false,
    );

    final result = await _iContentRepo.delete(value.content);

    yield state.copyWith(
      isSavingContent: false,
      isLoadingMainContentImage: false,
      mainContentImageUploadFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: some(result),
      saveFailureOrSuccessOption: none(),
    );
  }

  Stream<ContentFormState> _selectedForEditing(
      _EveSelectedForEditing value) async* {
    yield state.copyWith(
      content: value.content,
      saveFailureOrSuccessOption: none(),
    );
  }
}
