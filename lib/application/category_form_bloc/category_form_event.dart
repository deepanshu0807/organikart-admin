part of 'category_form_bloc.dart';

@freezed
abstract class CategoryFormEvent with _$CategoryFormEvent {
  const factory CategoryFormEvent.titleChanged(String title) = _EvTitleChanged;

  const factory CategoryFormEvent.shortDescriptionChanged(String description) =
      _EvShortDescriptionChanged;

  const factory CategoryFormEvent.titlePicUrlChanged(String titlePicUrl) =
      _EvTitlePicUrlChanged;

  const factory CategoryFormEvent.uploadTitleImageClicked() =
      _EvUploadTitleImageClicked;

  const factory CategoryFormEvent.initialize() = _EvInitialize;

  const factory CategoryFormEvent.saveIsClicked() = _EvSaveIsClicked;

  const factory CategoryFormEvent.deleteIsClicked(Category category) =
      _EveDeleteIsClicked;

  const factory CategoryFormEvent.selectedForEditing(Category category) =
      _EveSelectedForEditing;
}
