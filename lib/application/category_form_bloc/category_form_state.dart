part of 'category_form_bloc.dart';

@freezed
abstract class CategoryFormState with _$CategoryFormState {
  const factory CategoryFormState({
    @required Category category,
    @required bool isLoadingTitleImage,
    @required bool isSavingCategory,
    @required Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
    @required Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
    @required
        Option<Either<InfraFailure, UploadResult>>
            titleImageUploadFailureOrSuccessOption,
  }) = _CategoryFormState;

  factory CategoryFormState.initial() => CategoryFormState(
        category: Category(
          id: UniqueId(),
          title: '',
          titlePicUrl: '',
          shortDescription: '',
        ),
        isLoadingTitleImage: false,
        isSavingCategory: false,
        saveFailureOrSuccessOption: none(),
        deleteFailureOrSuccessOption: none(),
        titleImageUploadFailureOrSuccessOption: none(),
      );
}
