// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'category_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$CategoryFormEventTearOff {
  const _$CategoryFormEventTearOff();

// ignore: unused_element
  _EvTitleChanged titleChanged(String title) {
    return _EvTitleChanged(
      title,
    );
  }

// ignore: unused_element
  _EvShortDescriptionChanged shortDescriptionChanged(String description) {
    return _EvShortDescriptionChanged(
      description,
    );
  }

// ignore: unused_element
  _EvTitlePicUrlChanged titlePicUrlChanged(String titlePicUrl) {
    return _EvTitlePicUrlChanged(
      titlePicUrl,
    );
  }

// ignore: unused_element
  _EvUploadTitleImageClicked uploadTitleImageClicked() {
    return const _EvUploadTitleImageClicked();
  }

// ignore: unused_element
  _EvInitialize initialize() {
    return const _EvInitialize();
  }

// ignore: unused_element
  _EvSaveIsClicked saveIsClicked() {
    return const _EvSaveIsClicked();
  }

// ignore: unused_element
  _EveDeleteIsClicked deleteIsClicked(Category category) {
    return _EveDeleteIsClicked(
      category,
    );
  }

// ignore: unused_element
  _EveSelectedForEditing selectedForEditing(Category category) {
    return _EveSelectedForEditing(
      category,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CategoryFormEvent = _$CategoryFormEventTearOff();

/// @nodoc
mixin _$CategoryFormEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result titleChanged(String title),
    @required Result shortDescriptionChanged(String description),
    @required Result titlePicUrlChanged(String titlePicUrl),
    @required Result uploadTitleImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Category category),
    @required Result selectedForEditing(Category category),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result titleChanged(String title),
    Result shortDescriptionChanged(String description),
    Result titlePicUrlChanged(String titlePicUrl),
    Result uploadTitleImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Category category),
    Result selectedForEditing(Category category),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result titleChanged(_EvTitleChanged value),
    @required Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    @required Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    @required Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result titleChanged(_EvTitleChanged value),
    Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  });
}

/// @nodoc
abstract class $CategoryFormEventCopyWith<$Res> {
  factory $CategoryFormEventCopyWith(
          CategoryFormEvent value, $Res Function(CategoryFormEvent) then) =
      _$CategoryFormEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$CategoryFormEventCopyWithImpl<$Res>
    implements $CategoryFormEventCopyWith<$Res> {
  _$CategoryFormEventCopyWithImpl(this._value, this._then);

  final CategoryFormEvent _value;
  // ignore: unused_field
  final $Res Function(CategoryFormEvent) _then;
}

/// @nodoc
abstract class _$EvTitleChangedCopyWith<$Res> {
  factory _$EvTitleChangedCopyWith(
          _EvTitleChanged value, $Res Function(_EvTitleChanged) then) =
      __$EvTitleChangedCopyWithImpl<$Res>;
  $Res call({String title});
}

/// @nodoc
class __$EvTitleChangedCopyWithImpl<$Res>
    extends _$CategoryFormEventCopyWithImpl<$Res>
    implements _$EvTitleChangedCopyWith<$Res> {
  __$EvTitleChangedCopyWithImpl(
      _EvTitleChanged _value, $Res Function(_EvTitleChanged) _then)
      : super(_value, (v) => _then(v as _EvTitleChanged));

  @override
  _EvTitleChanged get _value => super._value as _EvTitleChanged;

  @override
  $Res call({
    Object title = freezed,
  }) {
    return _then(_EvTitleChanged(
      title == freezed ? _value.title : title as String,
    ));
  }
}

/// @nodoc
class _$_EvTitleChanged implements _EvTitleChanged {
  const _$_EvTitleChanged(this.title) : assert(title != null);

  @override
  final String title;

  @override
  String toString() {
    return 'CategoryFormEvent.titleChanged(title: $title)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EvTitleChanged &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(title);

  @override
  _$EvTitleChangedCopyWith<_EvTitleChanged> get copyWith =>
      __$EvTitleChangedCopyWithImpl<_EvTitleChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result titleChanged(String title),
    @required Result shortDescriptionChanged(String description),
    @required Result titlePicUrlChanged(String titlePicUrl),
    @required Result uploadTitleImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Category category),
    @required Result selectedForEditing(Category category),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return titleChanged(title);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result titleChanged(String title),
    Result shortDescriptionChanged(String description),
    Result titlePicUrlChanged(String titlePicUrl),
    Result uploadTitleImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Category category),
    Result selectedForEditing(Category category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (titleChanged != null) {
      return titleChanged(title);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result titleChanged(_EvTitleChanged value),
    @required Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    @required Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    @required Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return titleChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result titleChanged(_EvTitleChanged value),
    Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (titleChanged != null) {
      return titleChanged(this);
    }
    return orElse();
  }
}

abstract class _EvTitleChanged implements CategoryFormEvent {
  const factory _EvTitleChanged(String title) = _$_EvTitleChanged;

  String get title;
  _$EvTitleChangedCopyWith<_EvTitleChanged> get copyWith;
}

/// @nodoc
abstract class _$EvShortDescriptionChangedCopyWith<$Res> {
  factory _$EvShortDescriptionChangedCopyWith(_EvShortDescriptionChanged value,
          $Res Function(_EvShortDescriptionChanged) then) =
      __$EvShortDescriptionChangedCopyWithImpl<$Res>;
  $Res call({String description});
}

/// @nodoc
class __$EvShortDescriptionChangedCopyWithImpl<$Res>
    extends _$CategoryFormEventCopyWithImpl<$Res>
    implements _$EvShortDescriptionChangedCopyWith<$Res> {
  __$EvShortDescriptionChangedCopyWithImpl(_EvShortDescriptionChanged _value,
      $Res Function(_EvShortDescriptionChanged) _then)
      : super(_value, (v) => _then(v as _EvShortDescriptionChanged));

  @override
  _EvShortDescriptionChanged get _value =>
      super._value as _EvShortDescriptionChanged;

  @override
  $Res call({
    Object description = freezed,
  }) {
    return _then(_EvShortDescriptionChanged(
      description == freezed ? _value.description : description as String,
    ));
  }
}

/// @nodoc
class _$_EvShortDescriptionChanged implements _EvShortDescriptionChanged {
  const _$_EvShortDescriptionChanged(this.description)
      : assert(description != null);

  @override
  final String description;

  @override
  String toString() {
    return 'CategoryFormEvent.shortDescriptionChanged(description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EvShortDescriptionChanged &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(description);

  @override
  _$EvShortDescriptionChangedCopyWith<_EvShortDescriptionChanged>
      get copyWith =>
          __$EvShortDescriptionChangedCopyWithImpl<_EvShortDescriptionChanged>(
              this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result titleChanged(String title),
    @required Result shortDescriptionChanged(String description),
    @required Result titlePicUrlChanged(String titlePicUrl),
    @required Result uploadTitleImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Category category),
    @required Result selectedForEditing(Category category),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return shortDescriptionChanged(description);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result titleChanged(String title),
    Result shortDescriptionChanged(String description),
    Result titlePicUrlChanged(String titlePicUrl),
    Result uploadTitleImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Category category),
    Result selectedForEditing(Category category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (shortDescriptionChanged != null) {
      return shortDescriptionChanged(description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result titleChanged(_EvTitleChanged value),
    @required Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    @required Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    @required Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return shortDescriptionChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result titleChanged(_EvTitleChanged value),
    Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (shortDescriptionChanged != null) {
      return shortDescriptionChanged(this);
    }
    return orElse();
  }
}

abstract class _EvShortDescriptionChanged implements CategoryFormEvent {
  const factory _EvShortDescriptionChanged(String description) =
      _$_EvShortDescriptionChanged;

  String get description;
  _$EvShortDescriptionChangedCopyWith<_EvShortDescriptionChanged> get copyWith;
}

/// @nodoc
abstract class _$EvTitlePicUrlChangedCopyWith<$Res> {
  factory _$EvTitlePicUrlChangedCopyWith(_EvTitlePicUrlChanged value,
          $Res Function(_EvTitlePicUrlChanged) then) =
      __$EvTitlePicUrlChangedCopyWithImpl<$Res>;
  $Res call({String titlePicUrl});
}

/// @nodoc
class __$EvTitlePicUrlChangedCopyWithImpl<$Res>
    extends _$CategoryFormEventCopyWithImpl<$Res>
    implements _$EvTitlePicUrlChangedCopyWith<$Res> {
  __$EvTitlePicUrlChangedCopyWithImpl(
      _EvTitlePicUrlChanged _value, $Res Function(_EvTitlePicUrlChanged) _then)
      : super(_value, (v) => _then(v as _EvTitlePicUrlChanged));

  @override
  _EvTitlePicUrlChanged get _value => super._value as _EvTitlePicUrlChanged;

  @override
  $Res call({
    Object titlePicUrl = freezed,
  }) {
    return _then(_EvTitlePicUrlChanged(
      titlePicUrl == freezed ? _value.titlePicUrl : titlePicUrl as String,
    ));
  }
}

/// @nodoc
class _$_EvTitlePicUrlChanged implements _EvTitlePicUrlChanged {
  const _$_EvTitlePicUrlChanged(this.titlePicUrl) : assert(titlePicUrl != null);

  @override
  final String titlePicUrl;

  @override
  String toString() {
    return 'CategoryFormEvent.titlePicUrlChanged(titlePicUrl: $titlePicUrl)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EvTitlePicUrlChanged &&
            (identical(other.titlePicUrl, titlePicUrl) ||
                const DeepCollectionEquality()
                    .equals(other.titlePicUrl, titlePicUrl)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(titlePicUrl);

  @override
  _$EvTitlePicUrlChangedCopyWith<_EvTitlePicUrlChanged> get copyWith =>
      __$EvTitlePicUrlChangedCopyWithImpl<_EvTitlePicUrlChanged>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result titleChanged(String title),
    @required Result shortDescriptionChanged(String description),
    @required Result titlePicUrlChanged(String titlePicUrl),
    @required Result uploadTitleImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Category category),
    @required Result selectedForEditing(Category category),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return titlePicUrlChanged(titlePicUrl);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result titleChanged(String title),
    Result shortDescriptionChanged(String description),
    Result titlePicUrlChanged(String titlePicUrl),
    Result uploadTitleImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Category category),
    Result selectedForEditing(Category category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (titlePicUrlChanged != null) {
      return titlePicUrlChanged(titlePicUrl);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result titleChanged(_EvTitleChanged value),
    @required Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    @required Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    @required Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return titlePicUrlChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result titleChanged(_EvTitleChanged value),
    Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (titlePicUrlChanged != null) {
      return titlePicUrlChanged(this);
    }
    return orElse();
  }
}

abstract class _EvTitlePicUrlChanged implements CategoryFormEvent {
  const factory _EvTitlePicUrlChanged(String titlePicUrl) =
      _$_EvTitlePicUrlChanged;

  String get titlePicUrl;
  _$EvTitlePicUrlChangedCopyWith<_EvTitlePicUrlChanged> get copyWith;
}

/// @nodoc
abstract class _$EvUploadTitleImageClickedCopyWith<$Res> {
  factory _$EvUploadTitleImageClickedCopyWith(_EvUploadTitleImageClicked value,
          $Res Function(_EvUploadTitleImageClicked) then) =
      __$EvUploadTitleImageClickedCopyWithImpl<$Res>;
}

/// @nodoc
class __$EvUploadTitleImageClickedCopyWithImpl<$Res>
    extends _$CategoryFormEventCopyWithImpl<$Res>
    implements _$EvUploadTitleImageClickedCopyWith<$Res> {
  __$EvUploadTitleImageClickedCopyWithImpl(_EvUploadTitleImageClicked _value,
      $Res Function(_EvUploadTitleImageClicked) _then)
      : super(_value, (v) => _then(v as _EvUploadTitleImageClicked));

  @override
  _EvUploadTitleImageClicked get _value =>
      super._value as _EvUploadTitleImageClicked;
}

/// @nodoc
class _$_EvUploadTitleImageClicked implements _EvUploadTitleImageClicked {
  const _$_EvUploadTitleImageClicked();

  @override
  String toString() {
    return 'CategoryFormEvent.uploadTitleImageClicked()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _EvUploadTitleImageClicked);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result titleChanged(String title),
    @required Result shortDescriptionChanged(String description),
    @required Result titlePicUrlChanged(String titlePicUrl),
    @required Result uploadTitleImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Category category),
    @required Result selectedForEditing(Category category),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return uploadTitleImageClicked();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result titleChanged(String title),
    Result shortDescriptionChanged(String description),
    Result titlePicUrlChanged(String titlePicUrl),
    Result uploadTitleImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Category category),
    Result selectedForEditing(Category category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (uploadTitleImageClicked != null) {
      return uploadTitleImageClicked();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result titleChanged(_EvTitleChanged value),
    @required Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    @required Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    @required Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return uploadTitleImageClicked(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result titleChanged(_EvTitleChanged value),
    Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (uploadTitleImageClicked != null) {
      return uploadTitleImageClicked(this);
    }
    return orElse();
  }
}

abstract class _EvUploadTitleImageClicked implements CategoryFormEvent {
  const factory _EvUploadTitleImageClicked() = _$_EvUploadTitleImageClicked;
}

/// @nodoc
abstract class _$EvInitializeCopyWith<$Res> {
  factory _$EvInitializeCopyWith(
          _EvInitialize value, $Res Function(_EvInitialize) then) =
      __$EvInitializeCopyWithImpl<$Res>;
}

/// @nodoc
class __$EvInitializeCopyWithImpl<$Res>
    extends _$CategoryFormEventCopyWithImpl<$Res>
    implements _$EvInitializeCopyWith<$Res> {
  __$EvInitializeCopyWithImpl(
      _EvInitialize _value, $Res Function(_EvInitialize) _then)
      : super(_value, (v) => _then(v as _EvInitialize));

  @override
  _EvInitialize get _value => super._value as _EvInitialize;
}

/// @nodoc
class _$_EvInitialize implements _EvInitialize {
  const _$_EvInitialize();

  @override
  String toString() {
    return 'CategoryFormEvent.initialize()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _EvInitialize);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result titleChanged(String title),
    @required Result shortDescriptionChanged(String description),
    @required Result titlePicUrlChanged(String titlePicUrl),
    @required Result uploadTitleImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Category category),
    @required Result selectedForEditing(Category category),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return initialize();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result titleChanged(String title),
    Result shortDescriptionChanged(String description),
    Result titlePicUrlChanged(String titlePicUrl),
    Result uploadTitleImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Category category),
    Result selectedForEditing(Category category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialize != null) {
      return initialize();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result titleChanged(_EvTitleChanged value),
    @required Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    @required Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    @required Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return initialize(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result titleChanged(_EvTitleChanged value),
    Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initialize != null) {
      return initialize(this);
    }
    return orElse();
  }
}

abstract class _EvInitialize implements CategoryFormEvent {
  const factory _EvInitialize() = _$_EvInitialize;
}

/// @nodoc
abstract class _$EvSaveIsClickedCopyWith<$Res> {
  factory _$EvSaveIsClickedCopyWith(
          _EvSaveIsClicked value, $Res Function(_EvSaveIsClicked) then) =
      __$EvSaveIsClickedCopyWithImpl<$Res>;
}

/// @nodoc
class __$EvSaveIsClickedCopyWithImpl<$Res>
    extends _$CategoryFormEventCopyWithImpl<$Res>
    implements _$EvSaveIsClickedCopyWith<$Res> {
  __$EvSaveIsClickedCopyWithImpl(
      _EvSaveIsClicked _value, $Res Function(_EvSaveIsClicked) _then)
      : super(_value, (v) => _then(v as _EvSaveIsClicked));

  @override
  _EvSaveIsClicked get _value => super._value as _EvSaveIsClicked;
}

/// @nodoc
class _$_EvSaveIsClicked implements _EvSaveIsClicked {
  const _$_EvSaveIsClicked();

  @override
  String toString() {
    return 'CategoryFormEvent.saveIsClicked()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _EvSaveIsClicked);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result titleChanged(String title),
    @required Result shortDescriptionChanged(String description),
    @required Result titlePicUrlChanged(String titlePicUrl),
    @required Result uploadTitleImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Category category),
    @required Result selectedForEditing(Category category),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return saveIsClicked();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result titleChanged(String title),
    Result shortDescriptionChanged(String description),
    Result titlePicUrlChanged(String titlePicUrl),
    Result uploadTitleImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Category category),
    Result selectedForEditing(Category category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (saveIsClicked != null) {
      return saveIsClicked();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result titleChanged(_EvTitleChanged value),
    @required Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    @required Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    @required Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return saveIsClicked(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result titleChanged(_EvTitleChanged value),
    Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (saveIsClicked != null) {
      return saveIsClicked(this);
    }
    return orElse();
  }
}

abstract class _EvSaveIsClicked implements CategoryFormEvent {
  const factory _EvSaveIsClicked() = _$_EvSaveIsClicked;
}

/// @nodoc
abstract class _$EveDeleteIsClickedCopyWith<$Res> {
  factory _$EveDeleteIsClickedCopyWith(
          _EveDeleteIsClicked value, $Res Function(_EveDeleteIsClicked) then) =
      __$EveDeleteIsClickedCopyWithImpl<$Res>;
  $Res call({Category category});

  $CategoryCopyWith<$Res> get category;
}

/// @nodoc
class __$EveDeleteIsClickedCopyWithImpl<$Res>
    extends _$CategoryFormEventCopyWithImpl<$Res>
    implements _$EveDeleteIsClickedCopyWith<$Res> {
  __$EveDeleteIsClickedCopyWithImpl(
      _EveDeleteIsClicked _value, $Res Function(_EveDeleteIsClicked) _then)
      : super(_value, (v) => _then(v as _EveDeleteIsClicked));

  @override
  _EveDeleteIsClicked get _value => super._value as _EveDeleteIsClicked;

  @override
  $Res call({
    Object category = freezed,
  }) {
    return _then(_EveDeleteIsClicked(
      category == freezed ? _value.category : category as Category,
    ));
  }

  @override
  $CategoryCopyWith<$Res> get category {
    if (_value.category == null) {
      return null;
    }
    return $CategoryCopyWith<$Res>(_value.category, (value) {
      return _then(_value.copyWith(category: value));
    });
  }
}

/// @nodoc
class _$_EveDeleteIsClicked implements _EveDeleteIsClicked {
  const _$_EveDeleteIsClicked(this.category) : assert(category != null);

  @override
  final Category category;

  @override
  String toString() {
    return 'CategoryFormEvent.deleteIsClicked(category: $category)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EveDeleteIsClicked &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(category);

  @override
  _$EveDeleteIsClickedCopyWith<_EveDeleteIsClicked> get copyWith =>
      __$EveDeleteIsClickedCopyWithImpl<_EveDeleteIsClicked>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result titleChanged(String title),
    @required Result shortDescriptionChanged(String description),
    @required Result titlePicUrlChanged(String titlePicUrl),
    @required Result uploadTitleImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Category category),
    @required Result selectedForEditing(Category category),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return deleteIsClicked(category);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result titleChanged(String title),
    Result shortDescriptionChanged(String description),
    Result titlePicUrlChanged(String titlePicUrl),
    Result uploadTitleImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Category category),
    Result selectedForEditing(Category category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteIsClicked != null) {
      return deleteIsClicked(category);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result titleChanged(_EvTitleChanged value),
    @required Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    @required Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    @required Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return deleteIsClicked(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result titleChanged(_EvTitleChanged value),
    Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteIsClicked != null) {
      return deleteIsClicked(this);
    }
    return orElse();
  }
}

abstract class _EveDeleteIsClicked implements CategoryFormEvent {
  const factory _EveDeleteIsClicked(Category category) = _$_EveDeleteIsClicked;

  Category get category;
  _$EveDeleteIsClickedCopyWith<_EveDeleteIsClicked> get copyWith;
}

/// @nodoc
abstract class _$EveSelectedForEditingCopyWith<$Res> {
  factory _$EveSelectedForEditingCopyWith(_EveSelectedForEditing value,
          $Res Function(_EveSelectedForEditing) then) =
      __$EveSelectedForEditingCopyWithImpl<$Res>;
  $Res call({Category category});

  $CategoryCopyWith<$Res> get category;
}

/// @nodoc
class __$EveSelectedForEditingCopyWithImpl<$Res>
    extends _$CategoryFormEventCopyWithImpl<$Res>
    implements _$EveSelectedForEditingCopyWith<$Res> {
  __$EveSelectedForEditingCopyWithImpl(_EveSelectedForEditing _value,
      $Res Function(_EveSelectedForEditing) _then)
      : super(_value, (v) => _then(v as _EveSelectedForEditing));

  @override
  _EveSelectedForEditing get _value => super._value as _EveSelectedForEditing;

  @override
  $Res call({
    Object category = freezed,
  }) {
    return _then(_EveSelectedForEditing(
      category == freezed ? _value.category : category as Category,
    ));
  }

  @override
  $CategoryCopyWith<$Res> get category {
    if (_value.category == null) {
      return null;
    }
    return $CategoryCopyWith<$Res>(_value.category, (value) {
      return _then(_value.copyWith(category: value));
    });
  }
}

/// @nodoc
class _$_EveSelectedForEditing implements _EveSelectedForEditing {
  const _$_EveSelectedForEditing(this.category) : assert(category != null);

  @override
  final Category category;

  @override
  String toString() {
    return 'CategoryFormEvent.selectedForEditing(category: $category)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EveSelectedForEditing &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(category);

  @override
  _$EveSelectedForEditingCopyWith<_EveSelectedForEditing> get copyWith =>
      __$EveSelectedForEditingCopyWithImpl<_EveSelectedForEditing>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result titleChanged(String title),
    @required Result shortDescriptionChanged(String description),
    @required Result titlePicUrlChanged(String titlePicUrl),
    @required Result uploadTitleImageClicked(),
    @required Result initialize(),
    @required Result saveIsClicked(),
    @required Result deleteIsClicked(Category category),
    @required Result selectedForEditing(Category category),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return selectedForEditing(category);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result titleChanged(String title),
    Result shortDescriptionChanged(String description),
    Result titlePicUrlChanged(String titlePicUrl),
    Result uploadTitleImageClicked(),
    Result initialize(),
    Result saveIsClicked(),
    Result deleteIsClicked(Category category),
    Result selectedForEditing(Category category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectedForEditing != null) {
      return selectedForEditing(category);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result titleChanged(_EvTitleChanged value),
    @required Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    @required Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    @required Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    @required Result initialize(_EvInitialize value),
    @required Result saveIsClicked(_EvSaveIsClicked value),
    @required Result deleteIsClicked(_EveDeleteIsClicked value),
    @required Result selectedForEditing(_EveSelectedForEditing value),
  }) {
    assert(titleChanged != null);
    assert(shortDescriptionChanged != null);
    assert(titlePicUrlChanged != null);
    assert(uploadTitleImageClicked != null);
    assert(initialize != null);
    assert(saveIsClicked != null);
    assert(deleteIsClicked != null);
    assert(selectedForEditing != null);
    return selectedForEditing(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result titleChanged(_EvTitleChanged value),
    Result shortDescriptionChanged(_EvShortDescriptionChanged value),
    Result titlePicUrlChanged(_EvTitlePicUrlChanged value),
    Result uploadTitleImageClicked(_EvUploadTitleImageClicked value),
    Result initialize(_EvInitialize value),
    Result saveIsClicked(_EvSaveIsClicked value),
    Result deleteIsClicked(_EveDeleteIsClicked value),
    Result selectedForEditing(_EveSelectedForEditing value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectedForEditing != null) {
      return selectedForEditing(this);
    }
    return orElse();
  }
}

abstract class _EveSelectedForEditing implements CategoryFormEvent {
  const factory _EveSelectedForEditing(Category category) =
      _$_EveSelectedForEditing;

  Category get category;
  _$EveSelectedForEditingCopyWith<_EveSelectedForEditing> get copyWith;
}

/// @nodoc
class _$CategoryFormStateTearOff {
  const _$CategoryFormStateTearOff();

// ignore: unused_element
  _CategoryFormState call(
      {@required
          Category category,
      @required
          bool isLoadingTitleImage,
      @required
          bool isSavingCategory,
      @required
          Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      @required
          Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
      @required
          Option<Either<InfraFailure, UploadResult>>
              titleImageUploadFailureOrSuccessOption}) {
    return _CategoryFormState(
      category: category,
      isLoadingTitleImage: isLoadingTitleImage,
      isSavingCategory: isSavingCategory,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption,
      deleteFailureOrSuccessOption: deleteFailureOrSuccessOption,
      titleImageUploadFailureOrSuccessOption:
          titleImageUploadFailureOrSuccessOption,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CategoryFormState = _$CategoryFormStateTearOff();

/// @nodoc
mixin _$CategoryFormState {
  Category get category;
  bool get isLoadingTitleImage;
  bool get isSavingCategory;
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;
  Option<Either<InfraFailure, Unit>> get deleteFailureOrSuccessOption;
  Option<Either<InfraFailure, UploadResult>>
      get titleImageUploadFailureOrSuccessOption;

  $CategoryFormStateCopyWith<CategoryFormState> get copyWith;
}

/// @nodoc
abstract class $CategoryFormStateCopyWith<$Res> {
  factory $CategoryFormStateCopyWith(
          CategoryFormState value, $Res Function(CategoryFormState) then) =
      _$CategoryFormStateCopyWithImpl<$Res>;
  $Res call(
      {Category category,
      bool isLoadingTitleImage,
      bool isSavingCategory,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
      Option<Either<InfraFailure, UploadResult>>
          titleImageUploadFailureOrSuccessOption});

  $CategoryCopyWith<$Res> get category;
}

/// @nodoc
class _$CategoryFormStateCopyWithImpl<$Res>
    implements $CategoryFormStateCopyWith<$Res> {
  _$CategoryFormStateCopyWithImpl(this._value, this._then);

  final CategoryFormState _value;
  // ignore: unused_field
  final $Res Function(CategoryFormState) _then;

  @override
  $Res call({
    Object category = freezed,
    Object isLoadingTitleImage = freezed,
    Object isSavingCategory = freezed,
    Object saveFailureOrSuccessOption = freezed,
    Object deleteFailureOrSuccessOption = freezed,
    Object titleImageUploadFailureOrSuccessOption = freezed,
  }) {
    return _then(_value.copyWith(
      category: category == freezed ? _value.category : category as Category,
      isLoadingTitleImage: isLoadingTitleImage == freezed
          ? _value.isLoadingTitleImage
          : isLoadingTitleImage as bool,
      isSavingCategory: isSavingCategory == freezed
          ? _value.isSavingCategory
          : isSavingCategory as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      deleteFailureOrSuccessOption: deleteFailureOrSuccessOption == freezed
          ? _value.deleteFailureOrSuccessOption
          : deleteFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      titleImageUploadFailureOrSuccessOption:
          titleImageUploadFailureOrSuccessOption == freezed
              ? _value.titleImageUploadFailureOrSuccessOption
              : titleImageUploadFailureOrSuccessOption
                  as Option<Either<InfraFailure, UploadResult>>,
    ));
  }

  @override
  $CategoryCopyWith<$Res> get category {
    if (_value.category == null) {
      return null;
    }
    return $CategoryCopyWith<$Res>(_value.category, (value) {
      return _then(_value.copyWith(category: value));
    });
  }
}

/// @nodoc
abstract class _$CategoryFormStateCopyWith<$Res>
    implements $CategoryFormStateCopyWith<$Res> {
  factory _$CategoryFormStateCopyWith(
          _CategoryFormState value, $Res Function(_CategoryFormState) then) =
      __$CategoryFormStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Category category,
      bool isLoadingTitleImage,
      bool isSavingCategory,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
      Option<Either<InfraFailure, UploadResult>>
          titleImageUploadFailureOrSuccessOption});

  @override
  $CategoryCopyWith<$Res> get category;
}

/// @nodoc
class __$CategoryFormStateCopyWithImpl<$Res>
    extends _$CategoryFormStateCopyWithImpl<$Res>
    implements _$CategoryFormStateCopyWith<$Res> {
  __$CategoryFormStateCopyWithImpl(
      _CategoryFormState _value, $Res Function(_CategoryFormState) _then)
      : super(_value, (v) => _then(v as _CategoryFormState));

  @override
  _CategoryFormState get _value => super._value as _CategoryFormState;

  @override
  $Res call({
    Object category = freezed,
    Object isLoadingTitleImage = freezed,
    Object isSavingCategory = freezed,
    Object saveFailureOrSuccessOption = freezed,
    Object deleteFailureOrSuccessOption = freezed,
    Object titleImageUploadFailureOrSuccessOption = freezed,
  }) {
    return _then(_CategoryFormState(
      category: category == freezed ? _value.category : category as Category,
      isLoadingTitleImage: isLoadingTitleImage == freezed
          ? _value.isLoadingTitleImage
          : isLoadingTitleImage as bool,
      isSavingCategory: isSavingCategory == freezed
          ? _value.isSavingCategory
          : isSavingCategory as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      deleteFailureOrSuccessOption: deleteFailureOrSuccessOption == freezed
          ? _value.deleteFailureOrSuccessOption
          : deleteFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      titleImageUploadFailureOrSuccessOption:
          titleImageUploadFailureOrSuccessOption == freezed
              ? _value.titleImageUploadFailureOrSuccessOption
              : titleImageUploadFailureOrSuccessOption
                  as Option<Either<InfraFailure, UploadResult>>,
    ));
  }
}

/// @nodoc
class _$_CategoryFormState implements _CategoryFormState {
  const _$_CategoryFormState(
      {@required this.category,
      @required this.isLoadingTitleImage,
      @required this.isSavingCategory,
      @required this.saveFailureOrSuccessOption,
      @required this.deleteFailureOrSuccessOption,
      @required this.titleImageUploadFailureOrSuccessOption})
      : assert(category != null),
        assert(isLoadingTitleImage != null),
        assert(isSavingCategory != null),
        assert(saveFailureOrSuccessOption != null),
        assert(deleteFailureOrSuccessOption != null),
        assert(titleImageUploadFailureOrSuccessOption != null);

  @override
  final Category category;
  @override
  final bool isLoadingTitleImage;
  @override
  final bool isSavingCategory;
  @override
  final Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption;
  @override
  final Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption;
  @override
  final Option<Either<InfraFailure, UploadResult>>
      titleImageUploadFailureOrSuccessOption;

  @override
  String toString() {
    return 'CategoryFormState(category: $category, isLoadingTitleImage: $isLoadingTitleImage, isSavingCategory: $isSavingCategory, saveFailureOrSuccessOption: $saveFailureOrSuccessOption, deleteFailureOrSuccessOption: $deleteFailureOrSuccessOption, titleImageUploadFailureOrSuccessOption: $titleImageUploadFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CategoryFormState &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)) &&
            (identical(other.isLoadingTitleImage, isLoadingTitleImage) ||
                const DeepCollectionEquality()
                    .equals(other.isLoadingTitleImage, isLoadingTitleImage)) &&
            (identical(other.isSavingCategory, isSavingCategory) ||
                const DeepCollectionEquality()
                    .equals(other.isSavingCategory, isSavingCategory)) &&
            (identical(other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption)) &&
            (identical(other.deleteFailureOrSuccessOption,
                    deleteFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.deleteFailureOrSuccessOption,
                    deleteFailureOrSuccessOption)) &&
            (identical(other.titleImageUploadFailureOrSuccessOption,
                    titleImageUploadFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.titleImageUploadFailureOrSuccessOption,
                    titleImageUploadFailureOrSuccessOption)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(category) ^
      const DeepCollectionEquality().hash(isLoadingTitleImage) ^
      const DeepCollectionEquality().hash(isSavingCategory) ^
      const DeepCollectionEquality().hash(saveFailureOrSuccessOption) ^
      const DeepCollectionEquality().hash(deleteFailureOrSuccessOption) ^
      const DeepCollectionEquality()
          .hash(titleImageUploadFailureOrSuccessOption);

  @override
  _$CategoryFormStateCopyWith<_CategoryFormState> get copyWith =>
      __$CategoryFormStateCopyWithImpl<_CategoryFormState>(this, _$identity);
}

abstract class _CategoryFormState implements CategoryFormState {
  const factory _CategoryFormState(
      {@required
          Category category,
      @required
          bool isLoadingTitleImage,
      @required
          bool isSavingCategory,
      @required
          Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      @required
          Option<Either<InfraFailure, Unit>> deleteFailureOrSuccessOption,
      @required
          Option<Either<InfraFailure, UploadResult>>
              titleImageUploadFailureOrSuccessOption}) = _$_CategoryFormState;

  @override
  Category get category;
  @override
  bool get isLoadingTitleImage;
  @override
  bool get isSavingCategory;
  @override
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;
  @override
  Option<Either<InfraFailure, Unit>> get deleteFailureOrSuccessOption;
  @override
  Option<Either<InfraFailure, UploadResult>>
      get titleImageUploadFailureOrSuccessOption;
  @override
  _$CategoryFormStateCopyWith<_CategoryFormState> get copyWith;
}
