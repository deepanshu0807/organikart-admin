import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';

import 'package:meta/meta.dart';
import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/domain/category/i_category_repo.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'category_form_event.dart';
part 'category_form_state.dart';
part 'category_form_bloc.freezed.dart';

@injectable
class CategoryFormBloc extends Bloc<CategoryFormEvent, CategoryFormState> {
  final ICategoryRepo _iCategoryRepo;
  CategoryFormBloc(this._iCategoryRepo) : super(CategoryFormState.initial());

  @override
  Stream<CategoryFormState> mapEventToState(
    CategoryFormEvent event,
  ) async* {
    yield* event.map(
        titleChanged: _titleChanged,
        shortDescriptionChanged: _shortDescriptionChanged,
        titlePicUrlChanged: _titlePicUrlChanged,
        uploadTitleImageClicked: _uploadTitleImageClicked,
        initialize: _initialize,
        saveIsClicked: _saveIsClicked,
        deleteIsClicked: _deleteIsClicked,
        selectedForEditing: _selectedForEditing);
  }

  Stream<CategoryFormState> _titlePicUrlChanged(
      _EvTitlePicUrlChanged value) async* {
    yield state.copyWith(
      category: state.category.copyWith(
        titlePicUrl: value.titlePicUrl,
      ),
      isSavingCategory: false,
      isLoadingTitleImage: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      titleImageUploadFailureOrSuccessOption: none(),
    );
  }

  Stream<CategoryFormState> _uploadTitleImageClicked(
      _EvUploadTitleImageClicked value) async* {
    yield state.copyWith(
      isLoadingTitleImage: true,
      isSavingCategory: true,
    );

    yield state.copyWith(
      isLoadingTitleImage: true,
      isSavingCategory: true,
      saveFailureOrSuccessOption: none(),
      titleImageUploadFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
    );
    final result = await _iCategoryRepo.uploadTitleImage(state.category);

    yield state.copyWith(
      isLoadingTitleImage: false,
      isSavingCategory: false,
      titleImageUploadFailureOrSuccessOption: some(result),
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
    );

    result.fold(
      (l) {},
      (r) {
        add(CategoryFormEvent.titlePicUrlChanged(r.picUrl));
      },
    );
  }

  Stream<CategoryFormState> _titleChanged(_EvTitleChanged value) async* {
    yield state.copyWith(
      category: state.category.copyWith(
        title: value.title,
      ),
      isSavingCategory: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      titleImageUploadFailureOrSuccessOption: none(),
    );
  }

  Stream<CategoryFormState> _shortDescriptionChanged(
      _EvShortDescriptionChanged value) async* {
    yield state.copyWith(
      category: state.category.copyWith(
        shortDescription: value.description,
      ),
      isSavingCategory: false,
      saveFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: none(),
      titleImageUploadFailureOrSuccessOption: none(),
    );
  }

  Stream<CategoryFormState> _initialize(_EvInitialize value) async* {
    yield CategoryFormState.initial();
  }

  Stream<CategoryFormState> _saveIsClicked(_EvSaveIsClicked value) async* {
    yield state.copyWith(
      isSavingCategory: true,
      isLoadingTitleImage: true,
    );

    final result = await _iCategoryRepo.create(state.category);

    yield state.copyWith(
      isSavingCategory: false,
      isLoadingTitleImage: false,
      titleImageUploadFailureOrSuccessOption: none(),
      saveFailureOrSuccessOption: some(result),
      deleteFailureOrSuccessOption: none(),
    );

    result.fold(
      (l) {},
      (r) {
        add(const CategoryFormEvent.initialize());
      },
    );
  }

  Stream<CategoryFormState> _deleteIsClicked(_EveDeleteIsClicked value) async* {
    yield state.copyWith(
      isSavingCategory: false,
      isLoadingTitleImage: false,
    );

    final result = await _iCategoryRepo.delete(value.category);

    yield state.copyWith(
      isSavingCategory: false,
      isLoadingTitleImage: false,
      titleImageUploadFailureOrSuccessOption: none(),
      deleteFailureOrSuccessOption: some(result),
      saveFailureOrSuccessOption: none(),
    );
  }

  Stream<CategoryFormState> _selectedForEditing(
      _EveSelectedForEditing value) async* {
    yield state.copyWith(
      category: value.category,
      saveFailureOrSuccessOption: none(),
    );
  }
}
