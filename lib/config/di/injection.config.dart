// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:organikart_shared/infrastructure/category/category_repo.dart';
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:organikart_shared/domain/category/i_category_repo.dart';
import 'package:firebase/firebase.dart';

import '../../application/auth/auth_watcher_bloc/auth_watcher_bloc.dart';
import 'injectables.dart';
import '../../application/category_form_bloc/category_form_bloc.dart';
import '../../application/content_form_bloc/content_form_bloc.dart';
import '../../application/auth/signinform/signinform_bloc.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final blocInjectablemodule = _$BlocInjectablemodule();
  gh.factory<AuthWatcherBloc>(() => blocInjectablemodule.authWatcherBloc);
  gh.factory<CategoryWatcherBloc>(
      () => blocInjectablemodule.categoryWatcherBloc);
  gh.factory<ContentWatcherBloc>(() => blocInjectablemodule.contentWatcherBloc);
  gh.lazySingleton<FirebaseAuth>(() => blocInjectablemodule.fbAuth);
  gh.lazySingleton<FirebaseFirestore>(() => blocInjectablemodule.fStore);
  gh.lazySingleton<IAuth>(() => blocInjectablemodule.fbAuthService);
  gh.lazySingleton<ICategoryRepo>(() => blocInjectablemodule.categoryRepo);
  gh.lazySingleton<IContentRepo>(() => blocInjectablemodule.contentRepo);
  gh.factory<SigninformBloc>(() => SigninformBloc(get<IAuth>()));
  gh.lazySingleton<Storage>(() => blocInjectablemodule.storage);
  gh.factory<CategoryFormBloc>(() => CategoryFormBloc(get<ICategoryRepo>()));
  gh.factory<ContentFormBloc>(() => ContentFormBloc(get<IContentRepo>()));
  return get;
}

class _$BlocInjectablemodule extends BlocInjectablemodule {}
