import 'package:organikart_admin/application/auth/auth_watcher_bloc/auth_watcher_bloc.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart';
import 'package:organikart_shared/domain/category/i_category_repo.dart';
import 'package:organikart_shared/infrastructure/category/category_repo_web.dart';
import 'package:organikart_shared/infrastructure/content/content_repo_web.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

@module
abstract class BlocInjectablemodule {
  //Config

  //Externals
  @lazySingleton
  FirebaseAuth get fbAuth => FirebaseAuth.instance;

  @lazySingleton
  FirebaseFirestore get fStore => FirebaseFirestore.instance;

  @lazySingleton
  fb.Storage get storage => fb.storage();

  // Services
  @LazySingleton(as: IAuth)
  FirebaseAuthService get fbAuthService => FirebaseAuthService(fbAuth);

  @LazySingleton(as: ICategoryRepo)
  CategoryRepo get categoryRepo => CategoryRepo(fStore, storage);

  @LazySingleton(as: IContentRepo)
  ContentRepo get contentRepo => ContentRepo(fStore, storage);

  //Blocs
  @injectable
  AuthWatcherBloc get authWatcherBloc => AuthWatcherBloc(fbAuthService);

  @injectable
  CategoryWatcherBloc get categoryWatcherBloc =>
      CategoryWatcherBloc(categoryRepo);

  @injectable
  ContentWatcherBloc get contentWatcherBloc => ContentWatcherBloc(contentRepo);
}
