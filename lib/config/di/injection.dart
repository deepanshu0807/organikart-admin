import 'package:organikart_shared/organikart_shared_package.dart';

import 'injection.config.dart';

GetIt getIt = GetIt.instance;

@injectableInit
void configureInjection(String env) {
  $initGetIt(getIt, environment: env);
}
